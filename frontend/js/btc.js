var _module = angular.module('btc', []);

_module.constant('NANO_IN_BTC', Math.pow(10, 8));

_module.filter('asBTC', ['NANO_IN_BTC', function(NANO_IN_BTC) {
  return function(amountInNanocoins) {
  	if (!amountInNanocoins) amountInNanocoins = (0).toFixed(8);
  	var btc = (amountInNanocoins / NANO_IN_BTC).toFixed(8);

    btc = btc.replace(/0+$/,'').replace(/\.$/,''); // get rid of 0's at the end.
    if (btc) return btc;
    else return 0;
  };
}]);

_module.filter('asBTCFull', ['NANO_IN_BTC', function(NANO_IN_BTC) {
  return function(amountInNanocoins) {
  	if (!amountInNanocoins) amountInNanocoins = 0;
  	var amountInBTC = (amountInNanocoins / NANO_IN_BTC).toFixed(8);
  	return amountInBTC.split('.')[0];
  };
}]);

_module.filter('asBTCDecimal', ['NANO_IN_BTC', function(NANO_IN_BTC) {
  return function(amountInNanocoins) {
  	if (!amountInNanocoins) amountInNanocoins = 0;
  	var amountInBTC = (amountInNanocoins / NANO_IN_BTC).toFixed(8);

	var fractionalPart =  amountInBTC.toString().split('.')[1];
	fractionalPart = fractionalPart.replace(/0+$/,''); // get rid of 0's at the end.
	if (fractionalPart) return fractionalPart;
	else return 0;
  };
}]);
