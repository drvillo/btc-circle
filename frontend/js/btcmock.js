var _roulette = angular.module('roulette-btcmock', ['ngRoute', 'ngResource']);

_roulette.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {controller: 'BTCMockController', templateUrl: '/partials/btcmock.html'})
		.otherwise({redirectTo: '/'});
}]);

_roulette.factory('BTC', ['$resource', function($resource) {
	return $resource('/deposit', {}, {
		deposit: {method: 'POST', params: {}, isArray: false},
	});
}]);


_roulette.controller('BTCMockController', ['$scope', 'BTC',
 function($scope, BTC) {

 	$scope.NANO_IN_BTC = Math.pow(10, 8);

	$scope.deposit = function() {

		BTC.deposit({
			walletId: $scope.walletId,
			amount: $scope.amount * $scope.NANO_IN_BTC
		}, function() {
			console.log('OK');
		}, function(err) {
			console.error(err);
		});
		
	}

}]);
