angular.module('btcProcessor', ['ngResource', 'ui.router', 'ui.bootstrap.datetimepicker'])

.factory('BTCTransactions', ['$resource',
  function($resource) {
  return $resource('/btc/rest/public/transactions/:id/:aspect', {}, {
    search: {method: 'GET', params: {}, isArray: true},
    setNotificationState: {method: 'PUT', isArray: false}
  });
}])

.factory('BTCFrontWallets', ['$resource',
  function($resource) {
return $resource('/front_wallets', {}, {
    search: {method: 'GET', params: {}, isArray: true}
  });
}])

.factory('BTCFrontWalletsActions', ['$resource',
  function($resource) {
  return $resource('/btc/rest/private/front_wallets/:address/empty', {}, {
    empty: {method: 'PUT', params: {}, isArray: false}
  });
}])

.factory('BTCMainWallets', ['$resource',
  function($resource) {
  return $resource('/btc/rest/private/main_wallets/:id/:aspect', {}, {
    search: {method: 'GET', params: {}, isArray: true},
    toColdWallet: {method: 'POST', params: {aspect:'withdraw'}, isArray: false}
  });
}])

.filter('stateClass', function() {
  return function(state) {
    if (state === 'PENDING' || state==='NA') return 'label-warning';
    else if (state === 'FAILED') return 'label-danger';
    else return 'label-success';    
  }
})

.controller('BTCTransactionsController', ['$scope', '$stateParams', '$state', 'BTCTransactions',
  function($scope, $stateParams, $state, BTCTransactions) {

    $scope.params = $stateParams;


    if ($scope.params.fromTimestamp)
      $scope.params.fromTimestamp = new Date($scope.params.fromTimestamp);
    if ($scope.params.toTimestamp)
      $scope.params.toTimestamp = new Date($scope.params.toTimestamp);

    $scope.ledgers = ['IN', 'OUT', 'FEE'];
    $scope.states = ['PENDING', 'PEER_ANNOUNCED', 'BLOCKCHAIN', 'FAILED'];
    $scope.notificationStates = ['ACKNOWLEDGED', 'PENDING', 'FAILED', 
      'DONOTNOTIFY'];

    $scope.search = function(extra) {
      var params = angular.copy($scope.params);
      angular.extend(params, extra);

      if (params.fromTimestamp) params.fromTimestamp = params.fromTimestamp.toISOString();
      if (params.toTimestamp) params.toTimestamp = params.toTimestamp.toISOString();

      $state.transitionTo($state.current, params, {reload: true});
    }

    var params = angular.copy($scope.params);
    if (params.fromTimestamp) params.fromTimestamp = params.fromTimestamp.toISOString();
    if (params.toTimestamp) params.toTimestamp = params.toTimestamp.toISOString();
    
    $scope.transactions = BTCTransactions.search(params);
}])

.controller('BTCWalletsController', ['$scope', '$stateParams', '$state', 'BTCFrontWallets', 'BTCMainWallets', 'BTCFrontWalletsActions',
  function($scope, $stateParams, $state, BTCFrontWallets, BTCMainWallets, BTCFrontWalletsActions) {

    $scope.limit = 50; // wallets per page
    $scope.walletsOffset = 0;
    $scope.mainWalletsOffset = 0;


    $scope.params = $stateParams;

    $scope.toColdWallet = function(address,amount){

      var params={};
      params.amountNano=amount;
      BTCMainWallets.toColdWallet({id:address},params);

    }

    $scope.setAddressToColdWallet = function(address) {
    $scope.addressToColdWallet = angular.copy(address);
  }

    $scope.search = function(extra) {
      var params = angular.copy($scope.params);
      angular.extend(params, extra);

      console.log(params);
      $state.transitionTo($state.current, params, {reload: true});
    }

    $scope.empty = function(address) {
      BTCFrontWalletsActions.empty({address: address}, {}, function() {
        $scope.search();
      });
    }

    // front wallets
    var params = angular.copy($scope.params);
    params.limit = $scope.limit;
    params.offset = $scope.walletsOffset;
    $scope.wallets = BTCFrontWallets.search(params);

    // main wallets
    $scope.mainWallets = BTCMainWallets.search();
}])


;