var _roulette = angular.module('roulette', ['ngRoute', 'ngResource', 'ngCookies', 'ngTouch', 'btc']);

_roulette.run(['$rootScope', '$location', function ($rootScope, $location) {
    $rootScope.$location = $location;
    $rootScope.navigateTo = function(aspect) {
    	$location.search('aspect', aspect);
    }
    $rootScope.navigateHome = function() {
    	$location.search('aspect', null);
    }
}]);


_roulette.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {
  // Magic sauce, imediate so the value is stored and we don't need to lookup every check
  var _isNotMobile = (function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
      return !check;
  })();

  // var template = '/partials/roulette-mobile.html';
  var template = _isNotMobile ? '/partials/roulette.html' : '/partials/roulette-mobile.html';

  $routeProvider
		.when('/', {controller: 'RouletteController', templateUrl: template, reloadOnSearch: false})
		.when('/:aspect', {controller: 'RouletteController', templateUrl: template, reloadOnSearch: false})
		.otherwise({redirectTo: '/'});

    $httpProvider.responseInterceptors.push(['$q', '$rootScope', function($q, $rootScope) {

        function success(response) {
            return response;
        }

        function error(response) {
            var msg = ""
            if (response.status === 0) {
                msg = "You are not connected to the internet";
            } else if (response.status == 404) {
            	// we do not broadcast 404
            	return response;
        	} else {
                msg = response.data.message || "Unknown error"
            }
            $rootScope.$broadcast('event:error', msg);

            return $q.reject(response);
        }

        return function(promise) {
            return promise.then(success, error);
        }
    }]);
}]);

_roulette.filter('length', function() {
  return function(input) {
  	return input.length !== undefined ? input.length : Object.keys(input).length;
  };
});

_roulette.directive('wheel', ['NANO_IN_BTC', function(NANO_IN_BTC) {
  return {
    restrict: 'A',
    scope: {
      onClick: '=',
      circleRotationStyle: '=',
      wonAmount: '='
    },
    templateUrl: function(element, attr) { return attr.wheel },
    link: function(scope, element, attrs) {
      var btn = $(element).find("#button");

      btn.click(function() {
        scope.onClick();
      });

      var tapping = false;
      btn.bind('touchstart', function () {
          tapping = true;
          $(element).addClass("touching");
      });
      btn.bind('touchcancel', function () {
          tapping = false
          $(element).removeClass("touching");
      });
      btn.bind('touchend', function () {
        if (tapping) {
          scope.onClick();
          $(element).removeClass("touching");
        };
      });

      scope.$watch('circleRotationStyle', function() {
        $(element).find("#circle #wheel").css(scope.circleRotationStyle);
      });

      scope.$watch('wonAmount', function() {
        var v = scope.wonAmount / NANO_IN_BTC
        $(element).find("#circle #button #text #win-amount #second-line #shadow tspan").text(v);
        $(element).find("#circle #button #text #win-amount #second-line #text tspan").text(v);
      })
    }
  };
}]);


_roulette.directive('showOnStartup', ['$cookies', '$rootScope',
	function($cookies, $rootScope) {
    return {
        restrict: 'A',
         link: function ($scope, $element, $attrs) {
	    	var cookieName = $attrs.showOnStartup;
    		var value = $cookies[cookieName];

    		$rootScope.$on('terms:accepted', function() {
    			$cookies[cookieName] = '1';
    		});

    		$rootScope.$on('terms:rejected', function() {
    			$cookies[cookieName] = '0';
    			window.location = 'http://www.google.com';
    		});

    		if (value === undefined) {
    			$element.modal("show");
    		} else if (value !== '1') {
    			$rootScope.$broadcast('terms:rejected');
    		}

        }
    };
}]);

_roulette.directive('aspect', ['$rootScope', "$routeParams", "$location",
	function($rootScope, $routeParams, $location) {
    return {
        restrict: 'A',
         link: function ($scope, $element, $attrs) {
         	var aspectName = $attrs.aspect;

         	$rootScope.$on("$locationChangeSuccess", function(e) {
	         	var aspect = $location.search().aspect;
 	         	checkAspect(aspect);

 	         	$rootScope.$broadcast("aspect:" + aspect);
         	})

         	function checkAspect(aspect) {
    				if (aspect === aspectName) {
            console.log($element)
    					$element.modal("show");
    				} else {
    					$("body > .modal-backdrop").first().remove();
    					$element.modal("hide");
    				}
         	}

         	checkAspect($location.search().aspect);
        }
    };
}]);

_roulette.directive("scrollTo", ["$window", function($window){
    return {
      restrict : "AC",
      compile : function(){

        var document = $window.document;

        function scrollInto(idOrName) {//find element with the give id of name and scroll to the first element it finds
          if(!idOrName)
            $window.scrollTo(0, 0);
          //check if an element can be found with id attribute
          var el = document.getElementById(idOrName);
          if(!el) {//check if an element can be found with name attribute if there is no such id
            el = document.getElementsByName(idOrName);

            if(el && el.length)
              el = el[0];
            else
              el = null;
          }

          if(el) {
          	//if an element is found, scroll to the element
          	var tab = $("a[data-target='#" + idOrName + "']");
          	if (tab.length) {
          		tab.click();
	            document.getElementById('tabs').scrollIntoView();
          	} else {
	            el.scrollIntoView();
          	}
          }
          //otherwise, ignore
        }

        return function(scope, element, attr) {
          element.bind("click", function(event){
            scrollInto(attr.scrollTo);
          });
        };
      }
    };
}]);

_roulette.directive('datepicker', function () {
    return {
        restrict: 'A',
         link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
            	maxDate: 0,
                dateFormat: 'd/m/yy',
                onSelect: function (date) {
                    scope[attrs.datepicker] = element.datepicker("getDate");
                    scope.$apply();
                }
            });
        }
    };
});

_roulette.factory('Sounds', ['$rootScope', function($rootScope) {
	var spinSound = document.getElementById('spinSound');
	var winSound = document.getElementById('winSound');

	return {
		spin: function() {
			if (!spinSound || !spinSound.play) return;

      if (spinSound.currentTime)
  			spinSound.currentTime = 0;
			spinSound.play();
		},
		win: function() {
			if (!spinSound || !spinSound.play) return;
			spinSound.pause();

			if (!winSound || !winSound.play) return;
      if (winSound.currentTime)
  			winSound.currentTime = 0;
			winSound.play();
		}
	}
}]);


_roulette.factory('Socket', ['$rootScope', function ($rootScope) {
  var socket = io.connect();

  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
}]);

_roulette.factory('ClientSeedGenerator', [function() {
	return {
		generate: function() {
  			return '_' + Math.random(new Date().getTime()).toString(36);
		}
	}
}]);

_roulette.factory('Users', ['$resource', function($resource) {
	return $resource(':action', {}, {
		login: {method: 'POST', params: {action: 'login'}, isArray: false},
		logout: {method: 'POST', params: {action: 'logout'}, isArray: false},
		signup: {method: 'POST', params: {action: 'signup'}, isArray: false},
		setPersonalWallet: {method: 'PUT', params: {action: 'personal_wallet'}, isArray: false},
	});
}]);

_roulette.factory('Seeds', ['$resource', function($resource) {
	return $resource('/seed', {}, {
		getForTimestamp: {method: 'GET', isArray: false},
	});
}]);


_roulette.controller('NotificationsController', ['$scope', '$rootScope', '$timeout',
	function($scope, $rootScope, $timeout) {

		$rootScope.$on('event:error', function(event, message) {
			$timeout.cancel($scope.timeoutPromise);
			$scope.message = message;
			$scope.timeoutPromise = $timeout(function() {
				$scope.message = undefined;
			}, 10000);
		});

}]);

_roulette.controller('UserController', ['$scope', '$rootScope', 'Users', '$timeout', function($scope, $rootScope, Users, $timeout) {

	$scope.signup = function() {

		if ($scope.password !== $scope.passwordConfirmation) {
			$rootScope.$broadcast('event:error', 'Passwords do not match');
			return;
		}

		Users.signup({username: $scope.username, password: $scope.password}, function(response) {
			$scope.user = {
				name: response.name,
				id: response.id,
				wallet: response.wallet
			};
		}, function(err) {
			console.log(err);
		});
	}

	$scope.login = function() {
		Users.login({username: $scope.username, password: $scope.password}, function(response) {
			$scope.user = {
				name: response.name,
				id: response.id,
				wallet: response.wallet
			};
		}, function(err) {
			console.log(err);
		});
	}

	$scope.logout = function() {
		Users.logout({}, function(response) {
			window.location = '/';
		}, function(err) {
			console.log(err);
		});
	}

	// monitor secretUrl and set it back to original if changed.
	// this is to present it in a selectable input box and protect
	// from modifications.
	$scope.$watch('secretUrl', function() {
		if (!$scope.user) {
			$scope.secretUrl = "";
		} else {
			$scope.secretUrl = $scope.user.secret_url;
		}
	});


	$rootScope.$on("userDetails", function(e, user) {
		$scope.user = user;
		$scope.secretUrl = $scope.user.secret_url;
	});

	$rootScope.$on("event:error", function(e, message) {
		$scope.error = message;

		$timeout(function() {
			$scope.error = null;
		}, 2000);
	});

}]);

_roulette.factory('Casino', ['$resource', function($resource) {
	return $resource('/:action/:subaction', {}, {
		spin: {method: 'POST', params: {action: 'spin'}, isArray: false},
		withdraw: {method: 'POST', params: {action: 'withdraw'}, isArray: false},
		searchSpins: {method: 'GET', params: {action:'spins'}, isArray: true},
	});
}]);

_roulette.controller('WithdrawalController',
['$scope', '$rootScope', 'Casino', 'Users', 'NANO_IN_BTC',
function($scope, $rootScope, Casino, Users, NANO_IN_BTC) {

	$scope.amount = $scope.confirmedBalance / NANO_IN_BTC;
	$scope.editingAddress = false;

	$rootScope.$on('aspect:withdraw', function() {
		$scope.amount = $scope.confirmedBalance / NANO_IN_BTC;
	});

	$scope.close = function() {
		$rootScope.navigateHome();
	}

	$scope.editAddress = function() {
		$scope.editingAddress = true;
		$scope.newAddress = $scope.withdrawalAddress;
	}

	$scope.saveWithdrawalAddress = function() {
		$scope.editingAddress = false;

		Users.setPersonalWallet({wallet_id: $scope.newAddress},
			function(response) {
				$scope.withdrawalAddress = $scope.newAddress;
			});
	}

	$scope.withdraw = function() {
		var params = {
			destination_wallet_id: $scope.withdrawalAddress,
			amount: $scope.amount * NANO_IN_BTC
		};

		if ($scope.amount) {
			params.amount = $scope.amount * NANO_IN_BTC;
		}

		Casino.withdraw(params, function(result) {
			$scope.balance = result.balance;
		});

		$rootScope.navigateHome();
	}

}]);

_roulette.controller('RouletteController', ['$scope', '$rootScope',
		'$timeout', '$location', 'Socket', 'Casino', 'Users', 'Seeds',
		'ClientSeedGenerator', 'NANO_IN_BTC', 'Sounds', '$routeParams', '$sce',
 function($scope, $rootScope, $timeout, $location, Socket, Casino, Users, Seeds,
 	ClientSeedGenerator, NANO_IN_BTC, Sounds, $routeParams, $sce) {

 	$scope.aspect = $routeParams.aspect;

	$scope.spinning = false;
 	$scope.buttonImage = $sce.trustAsResourceUrl("/img/spinButton.png");

	// keeping two variables for spins & total: real + shadow
	// because we don't want to show updated recentSpins while
	// the wheel is spinning
	$scope.recentSpins = [];
	$scope.recentSpinsShadow = [];

	$scope.totalSpins = 0;
	$scope.totalSpinsShadow = 0;

	$scope.bet = 0.1;
	$scope.userId;
	$scope.balance = 0.0;
	$scope.clientSeed = ClientSeedGenerator.generate();

	$scope.circleId = 1;
  $scope.circleImageUrl = $sce.trustAsResourceUrl('/img/circle' + $scope.circleId + '.svg');

	$scope.circles = {
		1: '3x',
		2: '5x',
		3: '10x'
	};

	$scope.angle = 0;

  $scope.rightMenuVisible = false;
  $scope.toggleRightMenu = function() {
    console.dir($scope)
    $scope.rightMenuVisible = !$scope.rightMenuVisible;
  }
  $scope.leftMenuVisible = false;
  $scope.toggleLeftMenu = function() {
    $scope.leftMenuVisible = !$scope.leftMenuVisible;
  }

  $scope.overlayVisible = false;
  $scope.toggleOverlay = function() {
    $scope.overlayVisible = !$scope.overlayVisible;
  }

  $rootScope.$on("userDetails", function(e, user) {
    $scope.user = user;
  });

  $scope.fullScreen = false;
  $scope.toggleFullScreen = function() {
    $scope.fullScreen = !$scope.fullScreen;
  }


  $scope.wheelClasses = function() {
    var classes = [];
    classes.push('circle-' + $scope.circleId);

    if ($scope.disableSpinning) {
      classes.push('nospin');
    }

    if ($scope.spinning && !$scope.dummy) {
      classes.push('spinning');
    }

    if ($scope.spinning && $scope.dummy) {
      classes.push('deposit-now');
    }

    if ($scope.justWon) {
      classes.push('justwon');
    }

    return classes.join(' ');
  }

	var ButtonImageNormal = 'spinButton.png'
	  , ButtonImageHover = 'spinButtonHover.png'
	  , ButtonImageDeposit = 'spinButtonDepositNow.png';

	var updateSpinButton = function(force) {
		if ($scope.buttonImage === ButtonImageDeposit && !force) return;
		var i = $scope.hovering ? ButtonImageHover : ButtonImageNormal;
  	$scope.buttonImage = $sce.trustAsResourceUrl("/img/" + i);
  }

	$scope.$watch('hovering', function() {
		updateSpinButton();
	});
	$scope.hovering = false;

  $scope.klik = function() {
    console.log('KLIK')
  }

	$scope.setCircle = function(id) {
		if ($scope.spinning) return;

		if ($scope.circleId != id) {
			$scope.circleId = id;
      $scope.circleImageUrl = $sce.trustAsResourceUrl('/img/circle' + $scope.circleId + '.svg');

			$scope.disableSpinning = true;
			$scope.angle = 0;
			updateStyle();

			// we want to prevent wheel from spinning
			// when circles are changed. to enable spinning after
			// angular's "digest" where spinning was disabled,
			// we need to issue another "digest", for example in
			// a timeout
			$timeout(function() {
				$scope.disableSpinning = false;
			}, 100);
		}
	}

	var updateFromShadow = function() {
		if (!$scope.spinning) {
			$scope.recentSpins = $scope.recentSpinsShadow;
			$scope.totalSpins = $scope.totalSpinsShadow;
      adjustBet();
		}
	}

	var updateStyle = function() {
		var val = "rotate(" + $scope.angle + "deg)";

		$scope.circleStyle = {
			'transform': val,
			'-webkit-transform': val,
			'-ms-transform': val,
      '-moz-transform': val,
      '-o-transform': val,
		}
	}

  var adjustBet = function() {
    if ($scope.balance < $scope.parsedBet()) {
      // our bet is higher than remaining balance - adjust to max bet
      $scope.bet = 'max';
    }
    if ($scope.balance < (0.001 * NANO_IN_BTC)) {
      $scope.bet = 'max';
    }
  }

  $scope.$watch('balance', adjustBet);
  $scope.$watch('bet', adjustBet);

	$scope.rejectTerms = function() {
		$rootScope.$broadcast("terms:rejected");
	}

	$scope.acceptTerms = function() {
		$rootScope.$broadcast("terms:accepted");
	}

	$scope.resultLevel = function(result) {
		if (result == 0) {
			return 'zero';
		} else if (result > 0 && result < 2) {
			return 'one';
		} else if (result >= 1 && result < 3) {
			return 'two';
		} else {
			return 'three';
		}
	}

	$scope.proveSpin = function(spin) {
		$scope.provabilitySpin = spin;
		$scope.provabilityServerSeed = '';

		Seeds.getForTimestamp({
			timestamp: new Date(spin.timestamp).toISOString()
		}, function(response) {
			$scope.provabilityServerSeed = response.seed || response.message;
		});
	}

	$scope.parsedBet = function() {
		if (!$scope.bet) return 0;
		var uncappedbet=0;

		if ($scope.bet === 'min') {
			uncappedbet= 0.001 * NANO_IN_BTC;
		} else if ($scope.bet === 'max') {
			uncappedbet= $scope.balance;
		} else if (/.*%$/.test($scope.bet)) {
			var percentage = parseFloat($scope.bet.replace(/%$/, ''));
			uncappedbet= $scope.balance / (100/percentage);
		} else {
			var bet = parseFloat($scope.bet);
			if (isNaN(bet)) return 0;
			uncappedbet= bet * NANO_IN_BTC;
		}

    var cappedBet = uncappedbet;

		if (uncappedbet > 0.1 * NANO_IN_BTC){
			cappedBet = 0.1 * NANO_IN_BTC;
    } else if (uncappedbet < 0.001 * NANO_IN_BTC) {
      cappedBet = 0.001 * NANO_IN_BTC;
		}

    // do not show bets higher than balance
    return Math.min($scope.balance, cappedBet);
	}

	updateStyle();

	Socket.on('userDetails', function(details) {
		$scope.userId = details.id;
		$scope.balance = details.balance;

		$scope.confirmedBalance = details.confirmedBalance;
		$scope.unconfirmedBalance = details.unconfirmedBalance;

		$scope.reward = details.reward;
		$scope.userSpins = details.spins;
		$scope.depositAddress = details.wallet;
		$scope.withdrawalAddress = details.personal_wallet;

		updateFromShadow();

		$rootScope.$broadcast("userDetails", {
			id: details.id,
			name: details.name,
			wallet: details.wallet,
			personal_wallet: details.personal_wallet,
			secret_url: $location.absUrl().replace('#/', '') + '?secret=' + details.secret
		});
	});

	Socket.on('recentStats', function(data) {
		$scope.recentSpinsShadow = data.newResults.slice(0, 100);
		$scope.totalSpinsShadow = data.totalSpins;
		$scope.totalDonated = data.totalDonated;
		$scope.campaign = data.currentCharity;

		$scope.leaders = data.leaders;
		$scope.wleaders = data.wleaders;

		updateFromShadow();
	});

	Socket.on('dailySeedHash', function(hash) {
		$scope.dailySeedHash = hash;
	})


	Socket.on('newStats', function(data) {
		var joinedSpins = data.newResults.concat($scope.recentSpinsShadow);
		$scope.recentSpinsShadow = joinedSpins.slice(0, 100);
		$scope.totalSpinsShadow = data.totalSpins;
		$scope.leaders = data.leaders;
		$scope.wleaders = data.wleaders;
		$scope.totalDonated = data.totalDonated;
		$scope.campaign = data.currentCharity;

		updateFromShadow();
	});

	$scope.setWithdrawalAddress = function(address) {
    $scope.withdrawalAddress = address;

		Users.setPersonalWallet({wallet_id: $scope.withdrawalAddress},
			function(response) {
				$scope.depositAddress = response.wallet;
			});
	}

	$scope.setClientSeed = function() {
		$scope.clientSeed = $scope.newClientSeed;
		$scope.newClientSeed = undefined;
	}

	$scope.setBet = function(bet) {

		$scope.bet = bet;
	}

	$scope.spin = function() {
		if ($scope.spinning) return;

    $scope.justWon = false;

		Casino.spin({bet: $scope.parsedBet(), userId: $scope.userId, client_seed: $scope.clientSeed, circle_id: $scope.circleId}, function(result) {

			// see how many full circles the roulete made.
			var fullCircles = Math.floor($scope.angle / 360);

			// if the wheel is a little bit past the 360 mark, we need to spin
			// to the 360 mark plus the required angle plus the speedup
			var extraCircle = $scope.angle % 360 != 0 ? 1 : 0;

			// calculating the extra, needed to bring the roulette to the 360 mark
			var extra = (fullCircles + extraCircle) * 360  - $scope.angle;

			// extra full circles to mimick different force
			// var boost = Math.round(Math.random() * 3);
			var boost = 3; // keeping it constant for now

			$scope.spinning = true;
      $scope.dummy = result.dummy;

      $scope.won = result.returned;

			// new angle of the roulette is:
			// extra + speedup value (how many full circles it spins before stopping)
			// + the required angle.
			$scope.angle += extra + 360 * boost + result.angle;
			updateStyle();
			Sounds.spin();

			$timeout(function() {
				$scope.balance = result.balance;
        $scope.confirmedBalance = result.confirmedBalance;
        $scope.unconfirmedBalance = result.unconfirmedBalance;

        if (result.reward != undefined)
  				$scope.reward = result.reward;
				$scope.userSpins = result.totalSpins;

				$scope.spinning = false;
				updateSpinButton(true);

				Sounds.win();
				// now show buffered
				updateFromShadow();

        $scope.justWon = true;
        $timeout(function() {
          $scope.justWon = false;
        }, 3000);

			}, 5000);

		});
	}

	$scope.searchSpins = function() {
		if (!$scope.provabilityDate) return;

    var userId = '' + ($scope.provabilityUser || $scope.userId);
    if (userId && userId.trim().match(/^player /)) {
      userId = userId.replace('player', '').trim();
    }

		Casino.searchSpins({user_id: userId, date: $scope.provabilityDate.toISOString()},
			function(results) {
				$scope.provabilitySpins = results;
		});
	}
}]);
