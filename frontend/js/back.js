var _backend = angular.module('roulette-backend', ['ngResource', 'btc', 'btcProcessor', 'ui.router', 'ui.bootstrap']);

_backend.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}]);

_backend.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {


    $httpProvider.responseInterceptors.push(['$q', '$rootScope', function($q, $rootScope) {
    
        function success(response) {
            return response;
        }

        function error(response) {
            var msg = ""
            if (response.status === 0) {
                msg = "You are not connected to the internet";
        	} else {
                msg = response.data.message || "Unknown error"
            }
            $rootScope.$broadcast('event:error', msg);

            return $q.reject(response);
        }

        return function(promise) {
            return promise.then(success, error);
        }
    }]);   

	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state({
			name: 'dashboard',
			url: '/',
			controller: 'DashboardController', 
			templateUrl: '/partials/backend/dashboard.html'
		})
		.state({
			name: 'users',
			url: '/users',
			controller: 'UsersController', 
			templateUrl: '/partials/backend/users.html'
		})
		.state({
			name: 'transactions',
			url: '/transactions?id',
			controller: 'TransactionsController', 
			templateUrl: '/partials/backend/transactions.html'
		})
		.state({
			name: 'charities',
			url: '/charities',
			controller: 'CharitiesController', 
			templateUrl: '/partials/backend/charities.html'
		})
		.state({
			name: 'btc-transactions',
			url: '/btc/transactions?hash&ledger&state&notificationState&fromTimestamp&toTimestamp&address', 
			controller: 'BTCTransactionsController', 
			templateUrl: '/partials/backend/btcTransactions.html'
		})
		.state({
			name: 'unconfirmed-transactions',
			url: '/transactions/unconfirmed?period', 
			controller: 'UnconfirmedTransactionsController', 
			templateUrl: '/partials/backend/unconfirmedTransactions.html'
		}).state({
			name: 'btc-wallets',
			url: '/btc/wallets?requireAttention&address', 
			controller: 'BTCWalletsController', 
			templateUrl: '/partials/backend/wallets.html'
		});
}]);

_backend.factory('Charities', ['$resource', function($resource) {
	return $resource('/charities/:id/:aspect', {}, {
		all: {method: 'GET', params: {}, isArray: true},
		donate: {method: 'POST', params: {aspect: 'donations'}, isArray: false},
		add: {method: 'POST', params: {}, isArray: false},
		update: {method: 'PUT', params: {}, isArray: false},
		setCurrent: {method: 'POST', params: {aspect: 'current'}, isArray: false},
	});
}]);


_backend.factory('Donations', ['$resource', function($resource) {
	return $resource('/donations/:aspect', {}, {
		pending: {method: 'GET', params: {aspect: 'pending'}, isArray: false},
		total: {method: 'GET', params: {aspect: 'total'}, isArray: false},
	});
}]);


_backend.factory('Users', ['$resource', function($resource) {
	return $resource('/users/:id/:action', {}, {
		get: {method: 'GET', params: {}, isArray: true},
		stats: {method: 'GET', params: {action: 'stats'}, isArray: false},
		totalwithdrawn: {method: 'GET', params: {action: 'withdrawn'}, isArray: false},
		totaldeposited: {method: 'GET', params: {action: 'totaldeposited'}, isArray: false},
	});
}]);

_backend.factory('Transactions', ['$resource', function($resource) {
	return $resource('/transactions/:id/:action', {}, {
		search: {method: 'GET', params: {}, isArray: true},
		setStatus: {method: 'PUT', params: {action: 'status'}, isArray: false}
	});
}]);

_backend.factory('Deposits', ['$resource', function($resource) {
	return $resource('/bitcoin/:aspect', {}, {
		create: {method: 'POST', params: {aspect: 'deposit'}, isArray: false},
		confirm: {method: 'POST', params: {aspect: 'confirm'}, isArray: false},
	});
}]);


_backend.factory('Various', ['$resource', function($resource) {
	return $resource('/:aspect', {}, {
		profit: {method: 'GET', params: {aspect: 'profit'}, isArray: false},
		bonuses: {method: 'GET', params: {aspect: 'bonuses'}, isArray: false},
	});
}]);

_backend.factory('Balance', ['$resource', function($resource) {
	return $resource('/balance/:exclude', {}, {
		balance: {method: 'GET', params: {}, isArray: false},
	});
}]);


_backend.controller('NotificationsController', ['$scope', '$rootScope', '$timeout',
	function($scope, $rootScope, $timeout) {

		$rootScope.$on('event:error', function(event, message) {
			$timeout.cancel($scope.timeoutPromise);
			$scope.message = message;
			$scope.timeoutPromise = $timeout(function() {
				$scope.message = undefined;
			}, 10000);
		});

}]);

_backend.controller('MenuController', ['$scope', '$location', 
	function($scope, $location) {

	$scope.menu = [
		{name: 'Users', path: '/users'},
		{name: 'Transactions', path: '/transactions'},
		{name: 'Charities', path: '/charities'},
		{name: 'BTC transactions', path: '/btc/transactions'},
		{name: 'Unconfirmed', path: '/transactions/unconfirmed'},
		{name: 'Wallets', path: '/btc/wallets'}		
	];

	$scope.location = $location;
}]);


_backend.controller('DashboardController', ['$scope', 'Various', 'Users','BTCFrontWallets', 'BTCMainWallets', 'Balance',
	function($scope, Various, Users, BTCFrontWallets, BTCMainWallets, Balance) {

 	Various.profit({}, function(result) {
 		$scope.profit = result.amount;
 	});

 	Various.bonuses({}, function(result) {
 		$scope.bonuses = result.amount;
 	});

 	Balance.balance({exclude:true},{}, function(result) {
 		$scope.confirmed = result.amount;
 	});

 	Balance.balance({exclude:false},{}, function(result) {
 		$scope.total = result.amount;
 	});
	

 	BTCFrontWallets.search({}, function(result){
 		$scope.btcFrontTotalConfirmed = 0;
 		$scope.btcFrontTotalEstimated = 0;
 		nwallet=result.length;

 		for (var counter=0;counter<nwallet;counter++) {
 			$scope.btcFrontTotalConfirmed=$scope.btcFrontTotalConfirmed+result[counter].balanceNanoConfirmed;
 			$scope.btcFrontTotalEstimated=$scope.btcFrontTotalEstimated+result[counter].balanceNanoEstimated;
 		}
 	});

 	BTCMainWallets.search({}, function(result){
 		$scope.btcMainTotalConfirmed = 0;
 		$scope.btcMainTotalEstimated = 0;
 		nwallet=result.length;

 		for (var counter=0;counter<nwallet;counter++) {
 			$scope.btcMainTotalConfirmed=$scope.btcMainTotalConfirmed+result[counter].balanceNanoConfirmed;
 			$scope.btcMainTotalEstimated=$scope.btcMainTotalEstimated+result[counter].balanceNanoEstimated;
 		}
 	});

 	


 	$scope.usersStats = Users.stats();

}]);


_backend.controller('UsersController', ['$scope', 'Users', 
	function($scope, Users) {

	$scope.offset = 0;
	$scope.limit = 50;

 	var reload = function() {
	 	$scope.users = Users.get({
	 		filter: $scope.filter,
	 		offset: $scope.offset,
	 		limit: $scope.limit
	 	});
	 	}

	$scope.$watch('filter', function() {
		$scope.offset = 0;
		reload();
	});
	$scope.$watch('offset', reload);

	$scope.filter = 'active';


	$scope.totalwithdrawn = function(id){
		return Users.totalwithdrawn(id);
	}

	$scope.totaldeposited = function(id){
		return Users.totaldeposited(id);
	}

}]);

_backend.controller('TransactionsController', ['$scope', '$stateParams', 
	'Transactions', function($scope, $stateParams, Transactions) {

	$scope.offset = 0;
	$scope.limit = 50;

 	var reload = function() {
 		var q = {
	 		offset: $scope.offset,
	 		limit: $scope.limit
	 	};

	 	if ($scope.term) {
	 		q.term = $scope.term;
	 	}

	 	$scope.transactions = Transactions.search(q);
 	}

	$scope.$watch('term', function() {
		$scope.offset = 0;
		reload();
	});
	$scope.$watch('offset', reload);

	if ($stateParams.id)
		$scope.term = $stateParams.id;
	else 
		$scope.term = '';

}]);


_backend.controller('OldUsersController', ['$scope', '$timeout', 'Charities', 'Donations', 'Users', 'Various',
 function($scope, $timeout, Charities, Donations, Users, Various) {

 	Users.get({}, function(users) {
 		$scope.users = users;
 	});

 	Charities.all({}, function(charities) {
 		$scope.charities = charities;
 	});

 	Donations.pending({}, function(result) {
 		$scope.pendingDonations = result.amount;
 	});

 	Donations.total({}, function(result) {
 		$scope.totalDonated = result.amount;
 	});

 	Various.profit({}, function(result) {
 		$scope.profit = result.amount;
 	});


 	$scope.donate = function(charityId) {
 		console.log(charityId)
 		Charities.donate({id: charityId}, {}, function(result) {
 			console.log(result);
 			window.location = '/';
 		});
 	}

}]);


_backend.controller('CharitiesController', ['$scope', 'Charities', 'Donations',
	function($scope, Charities, Donations) {

	$scope.charities = Charities.all();

	$scope.edit = function(charity) {
		$scope.editedCharity = angular.copy(charity);
	}

	$scope.save = function() {
		if ($scope.editedCharity.id) {
			Charities.update({id: $scope.editedCharity.id}, $scope.editedCharity, function() {
				$scope.charities = Charities.all();
			});
		} else {
			Charities.add($scope.editedCharity, function() {
				$scope.charities = Charities.all();
			});			
		}
	}

	$scope.setCurrent = function(charity) {
		Charities.setCurrent({id: charity.id}, {}, function() {
			$scope.charities = Charities.all();			
		});
	}

	$scope.donate = function(charity) {
 		Charities.donate({id: charity.id}, {}, function() {
			updatePendingDonations();
 		});	
	}

	function updatePendingDonations() {
	 	Donations.pending({}, function(result) {
	 		$scope.pendingDonations = result.amount;
	 	});		
	}

	updatePendingDonations();

}]);

_backend.controller('UnconfirmedTransactionsController', ['$scope',
	'$stateParams', '$state', 'Transactions', 'BTCTransactions', 'Deposits', 
	function($scope, $stateParams, $state, Transactions, BTCTransactions, Deposits) {

	$scope.periods = ['day', 'week', 'month'];
	$scope.limit = 10;
	$scope.gameOffset = 0;
	$scope.btcOffset = 0;
	$scope.period = $stateParams.period;

	var toTimestamp = moment();
	var fromTimestamp = moment().subtract($scope.period + 's', 1);

	$scope.gameTransactions = Transactions.search({
		fromTimestamp: fromTimestamp.toISOString(),
		toTimestamp: toTimestamp.toISOString(),
		status: ['PENDING', 'FAILED'],
		offset: $scope.gameOffset,
		limit: $scope.limit
	});

	$scope.btcTransactions = BTCTransactions.search({
		fromTimestamp: fromTimestamp.toISOString(),
		toTimestamp: toTimestamp.toISOString(),
		notificationState: ['PENDING', 'FAILED'],
		offset: $scope.btcOffset,
		limit: $scope.limit
	});

	$scope.confirmGameTransaction = function(transaction) {
		Transactions.setStatus({id: transaction.transaction_id}, {status: 'CONFIRMED'}, function() {
			$state.transitionTo('transactions', {id: transaction.transaction_id});
		});
	}

	$scope.setProcessorTransactionToManual = function(transaction) {
		BTCTransactions.setNotificationState({id: transaction.id},
			{notificationState: 'MANUAL'}, function() {
				$state.transitionTo('btc-transactions', {hash: transaction.hash});
			});
	}

	$scope.createCorrespondingGameTransaction = function(transaction) {
		// 1. create game transaction
		Deposits.create({
			amount: transaction.value,
			walletId: transaction.toAddress,
			transactionId: transaction.hash
		}, function() {
			// 2. confirm game transaction
			Deposits.confirm({
				confirm: transaction.hash
			}, function() {
				// 3. set processor transaction to manual
				BTCTransactions.setNotificationState({id: transaction.id},
					{notificationState: 'MANUAL'}, function() {
						$state.transitionTo('transactions', {id: transaction.hash});
					});
			});
		});
	}

	$scope.$watch('period', function() {
		if ($scope.period != $stateParams.period)
			$state.transitionTo($state.current, {period: $scope.period}, {reload: true});
	});

	if (!$scope.period) {
		$scope.period = 'day';
	}

}]);
