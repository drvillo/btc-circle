module.exports = {
	db: {
		client: 'sqlite3',
		connection: {
			filename : ':memory:'
		}
	},
	port: 3000,
	backendPort: 3001,
	btcApiHost: 'localhost',
	btcApiPort: 3002,
	sessionCookieSecret: 'test',
	debug: true
}
