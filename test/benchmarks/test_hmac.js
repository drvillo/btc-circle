var Utils = require('./roulette/utils'),
	_ = require('lodash');

var buckets = {};

_.map(_.range(256), function(i) {
	buckets[i] = 0;
});

function getRandomNumberFromHmac(msg, seed) {
	var hmac = Utils.getSHA512Hmac(msg, seed);
	var randomNumber = parseInt(hmac.substring(0, 2), 16);
	return randomNumber;
}

var msg = 3000000;
var seed = 1000;

while(msg > 0) {
	while(seed > 0) {
		var r = getRandomNumberFromHmac('' + msg, '' + seed);
		buckets[r] += 1;
		seed -= 1;
	}
	msg -= 1;
	console.log(msg);
}

console.log(JSON.stringify(buckets));