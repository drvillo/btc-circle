process.env.ATALAYA_CIRCLE_CONFIG = './test/config';

var	Supertest = require('supertest')
  , expect = require('chai').expect
  , assert = require('assert')
  , Knex = require('knex')
  , Promise = require('bluebird')
  , Game = require('../app')
  , GameBackend = require('../back')
  , BTCMock = require('../btcmock')
  , Utils = require('../roulette/utils')
  , config = require('./config');
var app, backend, player, btcmock;

describe('Circle game', function() {

	before(function(done) {
		var db = Knex.initialize(config.db);

		// 1. start game
		Game(db).then(function(_app) {
			app = _app;
			// 2. start game backend
			return GameBackend(db);
		}).then(function(_backend) {
			backend = _backend;
			// 3. start bitcoin mock.
			return BTCMock(db);
		}).then(function(_btcmock) {
			btcmock = _btcmock;
			done();
		}).catch(done);
	});

	beforeEach(function(done) {
		player = Supertest.agent(app);

		// visit home page to obtain a new session ID.
		player.get('/').end(function(err, res) {
			if (err) return done(err);
			player.saveCookies(res);
			done();
		});
	});

	it('issues a front wallet in exchange for personal wallet', function(done) {
		player
			.put('/personal_wallet')
			.send({'wallet_id': '1GUMxbQC7Lb1pfrY1dSxCuY2uS7bTgJm6j'})
			.expect(200, function(err, res) {
				if (err) return done(err);

				expect(res.body.id).to.be.a('number');
				expect(res.body.wallet).to.be.a('string');
				app.locals.users.get(res.body.id).then(function(user) {
					expect(res.body.wallet).to.be.eql(user.wallet_id);
					done();
				}).catch(done);
		});
	});

	it('signs up anonymous user', function(done) {
		var username = 'foo';
		var password = 'barbazbzr';

		player
			.post('/signup')
			.send({username: username, password: password})
			.expect(200, function(err, res) {
				if (err) return done(err);

				expect(res.body.name).to.be.eql(username);
				app.locals.users.get(res.body.id).then(function(user) {
					expect(res.body.name).to.be.eql(user.name);
					done();
				}).catch(done);
			});
	});

	it('logs existing user in', function(done) {
		var username = 'bar';
		var password = '1234567';

		player
			.post('/signup')
			.send({username: username, password: password}).end(function(err, res) {
				if (err) return done(err);
				player
					.post('/login')
					.send({username: username, password: password})
					.expect(200, function(err, res) {
						if (err) return done(err);
						app.locals.users.get(res.body.id).then(function(user) {
							expect(res.body.name).to.be.eql(user.name);
							done();
						}).catch(done);
					})
			});
	});

	it('logs logged in user out', function(done) {
		var username = 'bar';
		var password = '1234567';
		
		player
			.post('/login')
			.send({username: username, password: password})
			.end(function(err, res) {
				if (err) return done(err);

				player
					.post('/logout')
					.expect(200, function(err, res) {
						expect(res.body.ok).to.be.true;
						done();
					})
			});
	});

	it('lets player spin the wheel without enough balance', function(done) {
		player
			.post('/spin')
			.send({client_seed: '123', bet: 1})
			.expect(200, function(err, res) {
				if (err) return done(err);

				expect(res.body.balance).to.be.eql(0);
				expect(res.body.dummy).to.be.true;
				done();
			});
	});

	it('lets player spin the wheel with balance', function(done) {
		// 1. set wallet id
		player
			.put('/personal_wallet')
			.send({'wallet_id': '1GUMxbQC7Lb1pfrY1dSxCuY2uS7bTgJm6j'}).expect(200, function(err, res) {
				if (err) return done(err);

				// 2. deposit some money
				app.locals.users.get(res.body.id).then(function(user) {
					return app.locals.casino.transferMoney(user, 10000000, '123', '123');
				}).then(function() {
					return app.locals.casino.confirmTransfer('123');
				}).then(function() {
					// 3. spin
					player
						.post('/spin')
						.send({client_seed: 'xx', bet: 1})
						.expect(200, function(err, res) {
							if (err) return done(err);
							expect(res.body.dummy).not.to.be.true;
							done();
						});
				}).catch(done);
			});
	});


	it('lets user withdraw', function(done) {
		var username = 'bar';
		var password = '1234567';
		
		var user;

		player
			.post('/login')
			.send({username: username, password: password})
			.end(function(err, res) {
				if (err) return done(err);

				app.locals.users.get(res.body.id).then(function(u) {
					user = u;
					return app.locals.casino.transferMoney(user, 10000000, '1234', '12345');
				}).then(function() {
					return app.locals.casino.confirmTransfer('123');
				}).then(function() {
					return Promise.join(
						app.locals.roulettes.getById(1),
						app.locals.dailySeeds.getCurrentSeed()
					);
				}).spread(function(roulette, dailySeed) {
					return app.locals.croupier.spin(roulette, user, 1, dailySeed, 'x');
				}).then(function() {
					player
						.post('/withdraw')
						.send({destination_wallet_id: 'yyy'})
						.expect(200, function(err, res) {
							if (err) return done(err);

							app.locals.casino.getBalanceForUser(user)
								.then(function(res) {
									expect(res.balance).to.be.eql(0);
									done();
								});
						});
				}).catch(done);
			});
	});

	it('does not let user withdraw if balance is zero', function(done) {
		player
			.post('/withdraw')
			.send({destination_wallet_id: 'yyy'})
			.expect(412, done);
	});
	

	it('validates bitcoin address', function(done) {
		var validAddresses = [
			"1AGNa15ZQXAZUgFiqJ2i7Z2DPU2J6hW62i",
			"1Q1pE5vPGEEMqRcVRMbtBK842Y6Pzo6nK9",
		];

		var invalidAddresses = [
			"1AGNa15ZQXAZUgFiqJ2i7Z2DPU2J6hW62X",
			"1ANNa15ZQXAZUgFiqJ2i7Z2DPU2J6hW62i",
			"1A Na15ZQXAZUgFiqJ2i7Z2DPU2J6hW62i",
		];

		validAddresses.forEach(function(address) {
			expect(Utils.isBTCAddressValid(address)).to.be.true;
		});

		invalidAddresses.forEach(function(address) {
			expect(Utils.isBTCAddressValid(address)).to.be.false;
		});

		done();
	})
});