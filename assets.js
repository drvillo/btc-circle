var _ = require('lodash')
  , Colors = require('colors')
  , less = require('less')
  , fs = require('fs')
  , UglifyJS = require("uglify-js");

/*
	Assets management.
*/

var GamesIDs = ['goodbet', 'orakle'];

var GameJSFiles = [
    "jquery.js",
    "jquery-ui.js",
    "bootstrap/modal.js",
    "bootstrap/tab.js",
    "socket.io.min.js",
    "angular.js",
    "angular-route.js",
    "angular-resource.js",
    "angular-cookies.js",
    "angular-touch.js",
    "btc.js",
    "app.js",
];

var CompressedJSFile = "game.js";

/*
	Compile Less game file into a compressed CSS file.
	E.g.:

		/frontend/less/coolcircle/circle.less
		to
		/frontend/css/coolcircle.css
*/
var compileCSS = function(callback) {
  var finished = 0;

	_.each(GamesIDs, function(gameId) {

		var lessParser = new(less.Parser)({
			paths: [
				__dirname + '/frontend/less/',
				__dirname + '/frontend/less/' + gameId,
			],
		});

		fs.readFile(__dirname + '/frontend/less/' + gameId + '/circle.less',
			function (err, data) {
			if (err) {
        console.error(err.message || err);
        throw err;
      }

			lessParser.parse(data.toString(), function(err, tree) {
        if (err) {
          console.error(err.message || err);
          throw err;
        }

				var css = tree.toCSS({compress: true});

				var outputFile = __dirname + '/frontend/css/' + gameId + '.css';
				fs.writeFile(outputFile, css,
					function(err) {
            if (err) {
              console.error(err.message || err);
              throw err;
            }

						console.log('Compiled LESS into %s'.green, outputFile);

            finished++;
						if (callback && finished == GamesIDs.length) callback();
				});

			});
		});


	});
}

var compileJS = function(callback) {
	var result = UglifyJS.minify(_.map(GameJSFiles, function(file) {
		return __dirname + '/frontend/js/' + file;
	}));

	var outputFile = __dirname + '/frontend/js/' + CompressedJSFile;

	fs.writeFile(outputFile, result.code,
		function(err) {
			if (err) throw err;
			console.log('Compiled JS into %s'.green, outputFile);
			if (callback) callback();
	});
}

module.exports.GameJSFiles = GameJSFiles;
module.exports.compileCSS = compileCSS;
module.exports.compileJS = compileJS;
module.exports.compile = function(callback) {
	compileCSS(function() {
		compileJS(callback);
	})
}

