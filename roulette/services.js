var Promise = require('bluebird');

function CasinoService(casino, users) {
	this.casino = casino;
	this.users = users;
}

CasinoService.prototype.getUserDetails = function(id) {	
	var details = {}
	  , self = this;
	
	return this.users.get(id).then(function(user) {
		if (!user) return;

		details.name = user.name;
		details.id = user.id;
		details.wallet = user.wallet_id;
		details.secret = user.secret;
		details.personal_wallet = user.personal_wallet_id;

		return Promise.join(
			self.casino.getBalanceForUser(user),
			self.casino.getBalanceForUser(user, true),
			self.casino.getTotalSpinsForUser(user.id)
		);
	}).spread(function(balance, confirmedBalance, spins) {
		if (!balance) return;

		details.balance = balance.balance ? balance.balance : 0;
		details.confirmedBalance = confirmedBalance.balance ? confirmedBalance.balance : 0;
		details.unconfirmedBalance = details.balance - details.confirmedBalance;

		details.reward = balance.reward;
		details.spins = spins ? spins : 0;

		return details;
	});	
}

module.exports.CasinoService = CasinoService;