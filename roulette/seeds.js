var Promise = require('bluebird')
	Utils = require('./utils'),
	moment = require('moment');

function tableCreator(t) {
	t.increments('id').primary();
	t.string('seed');
	t.string('hash');
	t.timestamp('timestamp').unique();
}

var DailySeeds = function(db, useLocalHashForNewSeed) {
	this.tableName = 'daily_seeds';
	this.db = db;
	this.localHash = useLocalHashForNewSeed;
	return this;
}


DailySeeds.prototype.q = function() {
	return this.db(this.tableName);
}

DailySeeds.prototype.getCurrentSeed = function() {
	return this.q().select().orderBy('id', 'desc').limit(1).then(function(results) {
		return results[0];
	});
}

DailySeeds.prototype.findForTimestamp = function(timestamp) {
	var startDate = moment(timestamp).startOf('day');
	var endDate = moment(timestamp).endOf('day');

	return this.q()
		.where('timestamp', '>=', startDate.toDate())
		.andWhere('timestamp', '<=', endDate.toDate())
		.then(function(results) {
			if (results) {
				return results[0];
			}
		});
}

DailySeeds.prototype.generateNewSeed = function() {
	var self = this;

	var hashGenerator = (this.localHash ?
		Promise.promisify(Utils.generateUUID)() :
		Utils.getRandomWithHashOfPage('http://www.reddit.com/r/bitcoin'))

	return hashGenerator
		.then(function(seed) {
			var record = {
				seed: seed,
				hash: Utils.hashString(seed),
				timestamp: moment().startOf('day').toDate()
			};
			return self.q().insert(record).then(function(results) {
				record.id = results[0];
				return record;
			});
		});
}

DailySeeds.prototype.getOldSeeds = function(n) {
	var q = this.q().select().orderBy('id', 'desc');
	q = n === undefined ? q : q.limit(n);
	return q.then(function(results) {
		// XXX I was trying to omit the first element with 'offset(1)'
		// but SQLite was throwing an error. This way is safer.
		return results.splice(1, results.length);
	});
}


module.exports = function(db, useLocalHashForNewSeed) {
	return Utils.createTableIfNeeded(db, 'daily_seeds', tableCreator)
		.then(function() {
			return new DailySeeds(db, useLocalHashForNewSeed);			
		});
}