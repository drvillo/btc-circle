

var Promise = require('bluebird'),
	_ = require('lodash'),
	Utils = require('./utils'),
	moment = require('moment'),
	fmt = require('util').format;

function tableCreator(t) {
	t.bigIncrements('id').primary();
	t.timestamp('timestamp').index();
	t.integer('user_id').index();
	t.enu('type', ['SPIN', 'TRANSFER']).index();
	t.bigInteger('dr');
	t.bigInteger('cr');

	// spin info
	t.string('session_id');
	t.integer('spin').unsigned().defaultTo(0);
	t.decimal('outcome');
	t.integer('sector').unsigned();
	t.integer('wheel_id');
	t.integer('daily_seed_id');
	t.string('client_seed');
	t.integer('reward').unsigned();

	// depsoit/withdrawal info
	t.enu('status', Statuses).index();
	t.string('transaction_id').index().unique();
	t.string('wallet_id');
	t.enu('transfer_type', ['DEPOSIT', 'WITHDRAWAL', 'BONUS']).index();
}

var Statuses = ['PENDING', 'CONFIRMED', 'FAILED'];


var Casino = function(db) {
	this.tableName = 'transactions';
	this.db = db;
	this.Statuses = Statuses;

	return this;
}

Casino.prototype.q = function() {
	return this.db(this.tableName);
}

Casino.prototype.getProfit = function() {
	return this.q().where('type', 'SPIN')
		.select(this.db.raw('sum(cr) as cr, sum(dr) as dr'))
		.then(function(results) {
			var dr = results[0]['dr'];
			var cr = results[0]['cr'];
			return dr - cr;
		});
}

Casino.prototype.getTotalBonusGranted = function() {
	return this.q().where('transfer_type', 'BONUS')
		.select(this.db.raw('sum(cr) as cr'))
		.then(function(results) {
			var cr = results[0]['cr'];
			return cr;
		});
}

/**
 * Get total spins happened in the casino since the beginning.
 */
Casino.prototype.getTotalSpins = function() {
	return this.q()
		.select(this.db.raw('count(id) as cnt'))
		.where('type', 'SPIN').then(function(result) {
		return result[0]['cnt'];
	});
}

/**
 * Get total spins for a user.
 */
Casino.prototype.getTotalSpinsForUser = function(id) {
	return this.q()
		.select(this.db.raw('count(id) as cnt'))
		.where('type', 'SPIN')
		.andWhere('user_id', id).then(function(result) {
			return result[0]['cnt'];
	});
}

/**
 * Get total BTC won by user.
 */
Casino.prototype.getTotalWonByUser = function(id) {
	return this.q()
		.select(this.db.raw('sum(cr) as cr, sum(dr) as dr'))
		.where('type', 'SPIN')
		.andWhere('user_id', id).then(function(result) {
			if (!result.length) return 0;
			var cr = result[0]['cr'];
			var dr = result[0]['dr'];
			return cr - dr;
	});
}

/*
	Returns amount of last bonus and sum of spin bets since the
	bonus was awarded:
	{
		bonus: 100000, // nano btc
		bets: 1000, // nano btc
	}

	or 'null' if no bonuses were awarded for this user.

	We do not allow users withdraw till they spend bonus amount.
*/
Casino.prototype.getTotalBetsSinceLastBonus = function(userId) {
	var context = {};
	var self = this;

	// 1. get latest bonus transfer
	return this.q().select(this.db.raw('id, cr'))
		.where('transfer_type', 'BONUS').andWhere('user_id', userId)
		.orderBy('id', 'desc').limit(1).then(function(results) {
			if (results.length == 0) return null;

			context.bonus = results[0].cr;
			var bonusId = results[0].id;

			return self.q().select(self.db.raw('sum(dr) as bets'))
				.where('type', 'SPIN').andWhere('user_id', userId)
				.andWhere('id', '>', bonusId);

		}).then(function(results) {
			if (!results || results.length == 0) return null;

			context.bets = results[0].bets;

			return context;
		});
}

/**
 * Get spin results happened since `timestamp` time.
 */
Casino.prototype.getResultsSince = function(timestamp) {
	return this.q()
		.join('users', 'transactions.user_id', '=', 'users.id', 'left')
		.where('type', 'SPIN')
		.andWhere('timestamp', '>', timestamp)
		.orderBy('timestamp', 'desc');
}

/**
 * Get `limit` number of recent spin results.
 */
Casino.prototype.getLatestResults = function(limit) {
	return this.q()
		.where('type', 'SPIN')
		.join('users', 'transactions.user_id', '=', 'users.id', 'left')
		.limit(limit)
		.orderBy('timestamp', 'desc');
}

Casino.prototype.getLeaders = function(limit) {
	return this.q()
		.where('type', 'SPIN')
		.join('users', 'transactions.user_id', '=', 'users.id', 'left')
		.select(this.db.raw('user_id as id, sum(cr) as winStreak, sum(dr) as loseStreak, count(transactions.id) as spins, name as username, sum(reward) as reward'))
		.groupBy('id')
		.orderBy('reward', 'desc')
		.limit(limit || 10);
}

Casino.prototype.getWeeklyLeaders = function(limit) {

	var oneWeekAgo = moment().subtract('days', 7);

	return this.q()
		.where('type', 'SPIN')
		.andWhere('timestamp', '>=', oneWeekAgo.toDate())
		.join('users', 'transactions.user_id', '=', 'users.id', 'left')
		.select(this.db.raw('user_id as id, sum(cr) as winStreak, sum(dr) as loseStreak, count(transactions.id) as spins, name as username, sum(reward) as reward'))
		.groupBy('id')
		.orderBy('reward', 'desc')
		.limit(limit || 10);
}

Casino.prototype.findSpins = function(userId, date) {
	var startDate = moment(date).startOf('day');
	var endDate = moment(date).endOf('day');

	return this.q()
		.join('users', 'transactions.user_id', '=', 'users.id', 'left')
		.where('type', 'SPIN')
		.andWhere('timestamp', '>=', startDate.toDate())
		.andWhere('timestamp', '<=', endDate.toDate())
		.andWhere(function() {
			this.where('user_id', '=', userId)
				.orWhere('users.name', '=', userId);
		});
}

/**
 * Returns user IDs.
 */
Casino.prototype.getUsersWithTransfersSince = function(timestamp) {
	return this.q()
		.where('type', 'TRANSFER')
		.andWhere('timestamp', '>=', timestamp)
		.groupBy('user_id')
		.select('user_id').then(function(results) {
			return _.map(results, function(r) { return r.user_id; });
		});
}

/**
 * Insert spin, but without spin details so far.
 * We need it to reserve the spin sequence number for current session ID.
 */
Casino.prototype.reserveSpin = function(user) {
	var self = this;

	return this.q().select(this.db.raw('count(id) as cnt'))
		.where('session_id', user.session_id)
		.andWhere('user_id', user.id)
		.then(function(result) {
			var transaction = {
				user_id: user.id,
				timestamp: new Date(),
				session_id: user.session_id,
				spin: result[0]['cnt']
			};

			return self.q().insert(transaction);

		}).then(function(result) {
			return self.q().where('id', result[0]).select();
		}).then(function(result) {
			return result[0];
		});
};

/**
 * Finalize spin: add all necessary details.
 */
Casino.prototype.finishSpin = function(spin) {
	var self = this;

	var spinData = _.clone(spin);
	spinData.type = 'SPIN';
	spinData.timestamp = new Date();

	return this.db.transaction(function(t) {

		// 1. update spin
		self.q().transacting(t)
			.where('id', spin.id)
			.update(spinData).then(function(result) {

				var balance = spin.cr - spin.dr;

				// 2. update user balance & rewards
				var q = self.db('users').transacting(t)
					.where('id', spin.user_id)
				var qr = self.db('users').transacting(t)
					.where('id', spin.user_id)
					.increment('rewards', spin.reward);

				if (balance > 0) q = q.increment("confirmed_balance", balance);
				else if (balance < 0) q = q.decrement("confirmed_balance", -balance);

				return Promise.join(q, qr);
			}).then(t.commit, t.rollback);

	}).then(function() {
		return self.q().where('id', spin.id).select();
	}).then(function(result) {
		return result[0];
	});

	// return this.q().where('id', spin.id).update(spinData).then(function(result) {
	// 	return self.q().where('id', spin.id).select();
	// }).then(function(result) {
	// 	return result[0];
	// });
}

/**
 * Get total Credit - Debit for the user. This includes spins,
 * deposits and withdrawals.
 * If 'excludePendingDeposits' flag is set, exclude initiated deposits
 * that have not been confirmed yet.
 * it is 'false' by default.
 */
Casino.prototype.getBalanceForUser = function(user, excludePendingDeposits) {
	var self = this;
	var builder = this.q()
		.select(this.db.raw('sum(cr) as cr, sum(dr) as dr, sum(reward) as reward'))
		.where('user_id', user.id);

	if (excludePendingDeposits === true) {
		builder = builder.whereNotIn('id', function() {
			this.select('id').from(self.tableName)
				.where('user_id', user.id)
				.andWhere('status', 'PENDING')
				.andWhere('dr', '=', 0);
		});
	}

	return builder.then(function(result) {
		var cr = result[0]['cr'];
		var dr = result[0]['dr'];
		var balance = cr - dr;
		return {
			balance: balance,
			reward: result[0]['reward']
		};
	});
}

Casino.prototype.getLastTransactionForUser = function(userId) {
	return this.q()
		.where('user_id', userId)
		.orderBy('id', 'desc')
		.limit(1)
		.then(function(response) {
			return response.length ? response[0] : null
		});
}

/**
 * Get aggregated Credit - Debit for all users. This includes spins,
 * deposits and withdrawals.
 * If 'excludePendingDeposits' flag is set, exclude initiated deposits
 * that have not been confirmed yet.
 * it is 'false' by default.
 */
Casino.prototype.getTotalBalance = function(excludePendingDeposits) {
	var self = this;
	var builder = this.q()
		.select(this.db.raw('sum(cr) as cr, sum(dr) as dr, sum(reward) as reward'))


	if (excludePendingDeposits === true) {
		builder = builder.whereNotIn('id', function() {
			this.select('id').from(self.tableName)
				.where('status', 'PENDING')
				.andWhere('dr', '=', 0);
		});
	}

	return builder.then(function(result) {
		var cr = result[0]['cr'];
		var dr = result[0]['dr'];
		var balance = cr - dr;
		return balance;
	});
}

/**
 * Deposit or withdraw the `amount` of money for the user.
 * If amount is < 0, it's a withdrawal, if it's > 0 - deposit.
 * This amount is marked as 'PENDING'.
 */
Casino.prototype.transferMoney = function(user, amount, transactionId, walletId) {
	var cr = amount >= 0 ? amount : 0;
	var dr = amount < 0 ? -amount : 0;

	var deposit = {
		user_id: user.id,
		timestamp: new Date(),
		type: 'TRANSFER',
		cr: cr,
		dr: dr,
		status: 'PENDING',
		transaction_id: transactionId,
		wallet_id: walletId,
		transfer_type: cr > 0 ? 'DEPOSIT' : 'WITHDRAWAL'
	}

	var self = this;


	return this.db.transaction(function(t) {
		return self.q().transacting(t).insert(deposit)
			.then(function() {

				var balance = deposit.cr - deposit.dr;

				// 2. update user balance
				var q = self.db('users').transacting(t)
					.where('id', deposit.user_id)

				if (balance > 0) q = q.increment("unconfirmed_balance", balance);
				else if (balance < 0) q = q.decrement("unconfirmed_balance", -balance);

				return q;
			})
			.then(t.commit, t.rollback)
			.then(function(result) {
				return self.getBalanceForUser(user);
			});
	});

	// return this.q().insert(deposit).then(function(result) {
	// 	return self.getBalanceForUser(user);
	// });
}

/**
 * Confirms that transaction has been successfull.
 * Returns a promise that resolves to true (udpated)
 * or false (no such transaction).
 */
Casino.prototype.confirmTransfer = function(transactionId) {

	var confirmed = false;
	var self = this;

	return this.db.transaction(function(t) {
		return self.q().transacting(t)
				.update({status: 'CONFIRMED', timestamp: new Date()})
				.where('transaction_id', transactionId)
				.andWhere('status', '<>', 'CONFIRMED')
				.andWhere('status', '<>', 'FAILED')
			.then(function(result) {
				if (result === undefined || result.length == 0) {
					confirmed = false;
				} else {
					confirmed = true;
				}
				return self.q().transacting(t).select()
					.where('transaction_id', transactionId);
			}).then(function(result) {
				if (result.length == 0) return;

				var transaction = result[0];

				// 2. update user balance
				var balance = transaction.cr - transaction.dr;

				// doing two queries because knex does not like two
				// increments in one query.

				var q1 = self.db('users').transacting(t)
					.where('id', transaction.user_id)
				var q2 = self.db('users').transacting(t)
					.where('id', transaction.user_id)

				if (balance > 0) {
					q1 = q1.increment("confirmed_balance", balance)
					q2 = q2.decrement("unconfirmed_balance", balance);
				} else {
					q1 = q1.decrement("confirmed_balance", -balance)
					q2 = q2.increment("unconfirmed_balance", -balance);
				}
				return Promise.join(q1, q2);
			})
			.then(t.commit, t.rollback)
			.then(function() {
				return confirmed;
			});
	});
}

/**
 * Confirm that transaction has failed.
 * Sets status to 'FAILED', dr and cr to the same amount.
 * Returns a promise that resolves to true (udpated)
 * or false (no such transaction).
 */
Casino.prototype.failTransfer = function(transactionId) {

	var self = this;

	var transaction;
	return this.db.transaction(function(t) {
		return self.q().transacting(t).where('transaction_id', transactionId)
			.then(function(results) {
				if (results !== undefined && results.length > 0) {
					transaction = results[0];

					var amount = transaction.dr > 0 ? transaction.dr : transaction.cr;
					return self.q().transacting(t)
						.update({
							status: 'FAILED',
							dr: amount,
							cr: amount,
							timestamp: new Date()
						})
						.where('transaction_id', transactionId)
				}
			}).then(function(result) {
				if (!result) return;

				var balance = transaction.cr - transaction.dr;

				var q = self.db('users').transacting(t)
					.where('id', transaction.user_id)

				if (balance > 0) {
					q = q.decrement("unconfirmed_balance", balance);
				} else {
					q = q.increment("unconfirmed_balance", -balance);
				}
				return q;
			}).then(t.commit, t.rollback);
	});
}

Casino.prototype.setTransactionStatus = function(transactionId, status) {
	var self = this;

	return this.q().where('transaction_id', transactionId)
		.then(function(results) {
			if (results.length == 0) {
				return null;
			} else {
				var transaction = results[0];
				if (transaction.status === 'FAILED') {
					if (transaction.transfer_type == 'DEPOSIT') {
						return self.q().where('transaction_id', transactionId)
							.update({
								'status': status,
								'dr': 0
							});
					} else {
						return self.q().where('transaction_id', transactionId)
							.update({
								'status': status,
								'cr': 0
							});
					}
				} else {
					return self.q().where('transaction_id', transactionId)
						.update({
							'status': status,
						});
				}
			}
		}).then(function(results) {
			return results ? true : false;
		});
}

var TransactionSearchParams = ['fromTimestamp', 'toTimestamp'];

Casino.prototype.searchTransactions = function(params) {
	var q = this.q().join('users', 'transactions.user_id', '=', 'users.id', 'left')
		.where('type', 'TRANSFER')

	// if there are any params that we search by
	if (_.intersection(TransactionSearchParams, _.keys(params))) {
		if (params.fromTimestamp)
			q = q.andWhere('timestamp', '>=', params.fromTimestamp);
		if (params.toTimestamp)
			q = q.andWhere('timestamp', '<=', params.toTimestamp);
		if (params.status) {
			if (typeof params.status === 'string')
				q = q.andWhere('status', '=', params.status);
			else {
				q = q.andWhere(function() {
					var self = this;
					_.each(params.status, function(status) {
						self = self.orWhere('status', '=', status);
					});
				})
			}

		}
	}

	if (params.limit) {
		q = q.limit(params.limit);
	}

	if (params.offset) {
		q = q.offset(params.offset);
	}

	return q.orderBy('timestamp', 'desc');
}

Casino.prototype.searchTransactionsByTerm = function(term, limit, offset) {
	var q = this.q().where('type', 'TRANSFER');

	if (term) {
		q = q.andWhere(function() {
			return this
				.orWhere('user_id', 'like', term)
				.orWhere('timestamp', 'like', term)
				.orWhere('dr', 'like', term)
				.orWhere('cr', 'like', term)
				.orWhere('status', 'like', term)
				.orWhere('transaction_id', 'like', term)
				.orWhere('wallet_id', 'like', term);
		});
	}

	if (limit) {
		q = q.limit(limit);
	}

	if (offset) {
		q = q.offset(offset);
	}

	return q.orderBy('timestamp', 'desc');
}

/**
 * Get total BTC deposited by user.
 */
Casino.prototype.getTotalDepositedByUser = function(id) {
	return this.q()
		.select(this.db.raw('sum(cr) as cr'))
		.where('type', 'TRANSFER')
		.andWhere('transfer_type', 'DEPOSIT')
		.andWhere('user_id', id).then(function(result) {
			if (!result.length || result[0]['cr']==null) return 0;
			var cr = result[0]['cr'];
			return cr;
	});
}

/**
 * Get total BTC withdrawn by user.
 */
Casino.prototype.getTotalWithDrawnByUser = function(id) {
	return this.q()
		.select(this.db.raw('sum(dr) as dr'))
		.where('type', 'TRANSFER')
		.andWhere('user_id', id).then(function(result) {
			if (!result.length || result[0]['dr']==null) return 0;
			var dr = result[0]['dr'];
			return dr;
	});
}

Casino.prototype.awardBonus = function(userId, bonus) {
	var deposit = {
		user_id: userId,
		timestamp: new Date(),
		type: 'TRANSFER',
		cr: bonus,
		dr: 0,
		status: 'CONFIRMED',
		transaction_id: 'bonus-' + Utils.generateUUID(),
		wallet_id: 'bonus',
		transfer_type: 'BONUS'
	}

	var self = this;

	return this.db.transaction(function(t) {

		return self.q().transacting(t)
		.insert(deposit)
		.then(function() {

			// update user balance
			return self.db('users').transacting(t)
				.where('id', deposit.user_id)
				.increment("confirmed_balance", bonus);

		}).then(t.commit, t.rollback);
	});

}


module.exports = function(db) {
	return Utils.createTableIfNeeded(db, 'transactions', tableCreator)
		.then(function() {
			return new Casino(db);
		});
}