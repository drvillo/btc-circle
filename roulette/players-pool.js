

function Pool(options) {
	this.userDetailsFactory = options.userDetailsFactory;
	this.errorHandler = options.errorHandler;
}

Pool.prototype.addPlayer = function(id, socket) {
	throw new Error('not implemented');
}

Pool.prototype.removePlayer = function(id) {
	throw new Error('not implemented');
}

Pool.prototype.getSocketForPlayer = function(id) {
	throw new Error('not implemented');	
}

Pool.prototype.announceDetails = function(id) {
	if (!this.userDetailsFactory) return;

	var self = this;
	var socket = this.getSocketForPlayer(id);
	this.userDetailsFactory(id).then(function(details) {
		if (details) {
			if (!socket) throw 'Unknown player';
			socket.emit('userDetails', details);
		}
	}).catch(function(err) {
		if (self.errorHandler) self.errorHandler(err);
		else console.error('Could not announce user %s details: %s', id, err);
	})
}

function MemoryPool() {
	Pool.apply(this, Array.prototype.slice.call(arguments));
	this.store = {};
}

MemoryPool.prototype = new Pool({});

MemoryPool.prototype.addPlayer = function(id, socket) {
	this.store[id] = socket;
}

MemoryPool.prototype.removePlayer = function(id, socket) {
	delete this.store[id];
}

MemoryPool.prototype.getSocketForPlayer = function(id) {
	return this.store[id];
}

module.exports.MemoryPool = MemoryPool;
