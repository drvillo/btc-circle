var Promise = require('bluebird'),
	Utils = require('./utils'),
	APIClient = require('./api-client').APIClient;

var BTCProcessorClient = function(url,merchantKey) {
	this.client = new APIClient(url,merchantKey);
	return this;
};


BTCProcessorClient.prototype.getNewWalletId = function() {
	return this.client.get('/rest/public/front_wallets/next', {}).then(function(response) {
		if (!response.ok || !response.body.address) throw "No walletId returned from BTC Processor";
		return response.body;
	});
};

BTCProcessorClient.prototype.withdraw = function(amount, frontWalletId, destinationWalletId, transactionId) {
	return this.client.post('/rest/public/front_wallets/' + frontWalletId + '/withdrawals', {
		frontWalletAddress: frontWalletId,
		destinationAddress: destinationWalletId,
		amountNano: amount,
		transactionId: transactionId
	}).then(function(response) {
		if (!response.ok || !response.body || !response.body.transactionId) 
			throw "No transactionId returned from BTC Processor: " +
			(response.body ? response.body.message : "");

		return response.body.transactionId;
	});
};

module.exports = function(url,merchantKey) {
	return new BTCProcessorClient(url,merchantKey);
}