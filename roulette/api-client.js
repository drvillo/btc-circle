var Promise = require('bluebird'),
	querystring = require('querystring'),
	request = require('request-json');


var APIClient = function(url,merchantKey) {
	var options = {
			headers: {'x-nodebit-mkey':merchantKey}
		};
	this.client = request.newClient(url,options);
	return this;
};


APIClient.prototype.get = function(url, query) {
	var resolver = Promise.defer();
	var fullUrl = url + '?' + querystring.stringify(query);

	this.client.get(fullUrl, function(err, res, body) {
		if (err) {
			resolver.reject(err);
		} else {
			resolver.resolve({
				ok: (res.statusCode >= 200 && res.statusCode < 400),
				status: res.statusCode,
				body: body
			});
		}
	});

	return resolver.promise;
}

APIClient.prototype.post = function(url, data) {
	var resolver = Promise.defer();

	this.client.post(url, data, function(err, res, body) {
		if (err) {
			resolver.reject(err);
		} else {
			resolver.resolve({
				ok: (res.statusCode >= 200 && res.statusCode < 400),
				status: res.statusCode,
				body: body
			});
		}
	});

	return resolver.promise;
}

module.exports.APIClient = APIClient;