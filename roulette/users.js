var Promise = require('bluebird'),
	bcrypt = require('bcrypt'),
	_ = require('lodash'),
	Utils = require('./utils');

function tableCreator(t) {
	t.bigIncrements('id').primary();
	t.string('name').unique();
	t.timestamp('created_at').defaultTo(t.knex.raw('CURRENT_TIMESTAMP'));
	t.string('password_hash');
	t.string('session_id').notNullable(); // daily session ID.
	t.string('secret').notNullable().unique().index(); // secret ID.
	t.string('wallet_id').unique().index();
	t.string('wallet_hostname');
	t.bigInteger('confirmed_balance').defaultTo(0);
	t.bigInteger('unconfirmed_balance').defaultTo(0);
	t.integer('rewards').unsigned().defaultTo(0);
	t.string('personal_wallet_id'); // user's own wallet ID. the one he'd use to deposit from.
}

var Users = function(db) {
	this.tableName = 'users';
	this.db = db;
	return this;
}

Users.prototype.q = function() {
	return this.db(this.tableName);
}

Users.prototype.verifyPassword = function(user, password) {
	var resolver = Promise.defer();

	bcrypt.compare(password, user.password_hash, function(err, isMatch) {
        if (err) {
        	resolver.reject(err);
        } else {
        	resolver.resolve(isMatch);
        }
    });

    return resolver.promise;
}

Users.prototype.signUp = function(user, username, password) {
	var resolver = Promise.defer();
	var self = this;

	bcrypt.genSalt(10, function(err, salt) {
        if (err) {
        	return resolver.reject(err);
        } else {
			bcrypt.hash(password, salt, function(err, hash) {
				if (err) {
					resolver.reject(err);
				} else {
					self.q().where('id', user.id).update({
						name: username,
						password_hash: hash
					}).then(function() {
						resolver.resolve(true);
					}).catch(function(err) {
						resolver.reject(err);
					});
				}
			});
        }
    });

	return resolver.promise;
}

/**
 * Returns a promise that resolves to user object if user is found
 * or 'undefined' if no such user exists.
 */
Users.prototype.get = function(id) {
	return this.q().where('id', id).then(function(results) {
		if (results.length > 0) {
			return results[0];
		}
	});
}

Users.prototype.getBySecret = function(secret) {
	return this.q().where('secret', secret).then(function(results) {
		if (results.length > 0) {
			return results[0];
		}
	});
}

Users.prototype.getByName = function(name) {
	return this.q().where('name', name).then(function(results) {
		if (results.length > 0) {
			return results[0];
		}
	});
}

Users.prototype.getByWalletId = function(id) {
	return this.q().where('wallet_id', id).then(function(results) {
		return results[0];
	});
}

/**
 * Returns a promise that resolves to a new user object.
 */
Users.prototype.create = Promise.method(function(walletId) {
	var newUser = {
		session_id: Utils.generateSessionId(),
		wallet_id: walletId,
		secret: Utils.generateSessionId(),
	};

	return this.q().insert(newUser).then(function(result) {
		newUser.id = result[0];
		return newUser;
	});

});

Users.prototype.updatePersonalWallet = function(userId, walletId) {
	var self = this;
	return this.q().where('id', userId)
		.update({personal_wallet_id: walletId}).then(function() {
			return self.get(userId);
		});
}

Users.prototype.updateFrontWallet = function(userId, walletAddress, walletHostname) {
	var self = this;
	return this.q().where('id', userId)
		.update({
			wallet_id: walletAddress,
			wallet_hostname: walletHostname,
		}).then(function() {
			return self.get(userId);
		});
}

Users.prototype.getBalanceForUser = function(userId, excludePendingDeposits) {
	return this.q().where('id', userId).then(function(results) {
		var u = results.length > 0 ? results[0] : {};
		if (excludePendingDeposits) {
			return u.confirmed_balance;
		} else {
			return u.confirmed_balance + u.unconfirmed_balance;
		}
	});
}

Users.prototype.getAll = function(limit, offset) {
	var query = this.q()
		.join('transactions', 'users.id', '=', 'transactions.user_id', 'left')
		.select(this.db.raw('users.id as id, users.name as name,\
		 users.wallet_id as wallet_id, sum(transactions.cr) as cr, \
		 sum(transactions.dr) as dr, \
		 max(transactions.timestamp) as last_activity, \
		 count(transactions.timestamp) as total_activities, \
		 users.unconfirmed_balance as unconfirmed_balance, \
		 users.confirmed_balance as confirmed_balance, \
		 users.rewards as rewards'))
		.whereNotNull('users.wallet_id')
		.groupBy('users.id');

	if (limit) {
		query = query.limit(limit);
	}

	if (offset) {
		query = query.offset(offset);
	}

	return query;
}

Users.prototype.getByFrontWalletIds = function(walletIds) {
	return this.q().whereIn('users.wallet_id', walletIds)
		.join('transactions', 'users.id', '=', 'transactions.user_id', 'left')
		.select(this.db.raw('users.id as user_id, users.name as username, users.wallet_id as front_wallet_id, sum(transactions.cr) as cr, sum(transactions.dr) as dr'))
		.groupBy('users.id');
}

Users.prototype.getLastTransactionsByFrontWalletIds = function(walletIds) {
	return this.q().whereIn('users.wallet_id', walletIds).andWhere('transactions.type', 'TRANSFER')
		.join('transactions', 'users.id', '=', 'transactions.user_id', 'left')
		.select(this.db.raw('users.wallet_id as front_wallet_id, transactions.cr as cr, transactions.dr as dr, transactions.transaction_id as transaction_id, transactions.transfer_type as transfer_type, transactions.wallet_id as destination, transactions.status as status'))
		.orderBy('transactions.id')
		.groupBy('users.id');
}

/*
	Users who entered a wallet address but never deposited
	or span the wheel.
*/
Users.prototype.getInactive = function(limit, offset) {
	return this.getAll(limit, offset)
		.having('total_activities', '=', 0);
}

/*
	Users who at least deposited.
*/
Users.prototype.getActive = function(limit, offset) {
	return this.getAll(limit, offset)
		.having('total_activities', '>', 0);
}

/*
	See above.
*/
Users.prototype.getActiveCount = function() {
	return this.db('transactions')
		.select(this.db.raw('user_id'))
		.groupBy('user_id')
		.then(function(results) {
			return results.length;
		});
}

/*
	See above.
*/
Users.prototype.getInactiveCount = function() {
	return this.q()
		.select('id')
		.whereNotNull('wallet_id')
		.andWhere(function() {
			this.whereRaw('id not in (select user_id from transactions group by user_id)');
		})
		.then(function(results) {
			return results.length;
		});
}

Users.prototype.updateDailySessionIds = function() {
	var self = this;
	return this.q().select('id').then(function(users) {
		return Promise.all(_.map(users, function(user) {
			return self.q().update({
				session_id: Utils.generateSessionId()
			}).where('id', user.id);
		}));
	});
}

Users.prototype.removeOrphansOlderThan = function(date) {
	return this.q().del()
		.whereNull('wallet_id')
		.andWhere('created_at', '<', date);
}

module.exports = function(db) {
	return Utils.createTableIfNeeded(db, 'users', tableCreator).then(function() {
		return new Users(db);
	})
}