var Utils = require('./utils')
	, _ = require('lodash')
	, Promise = require('bluebird')
	, NodeUtil = require('util');

var fmt = NodeUtil.format;

function insertCharity(db, tableName, name, shortDescription, longDescription, imageUrl) {
	return db(tableName).insert({
		name: name,
		short_description: shortDescription,
		long_description: longDescription,
		image_url: imageUrl,
		current: true
	});
}

function charitiesTableCreator(t) {
	t.increments('id').primary();
	t.string('name').unique();
	t.string('short_description');
	t.string('long_description');
	t.string('image_url');
	t.boolean('current');
}

function donationsTableCreator(t) {
	t.increments('id').primary();
	t.timestamp('timestamp').index();
	t.integer('spin_id_ending').notNullable();
	t.integer('charity_id').notNullable();
	t.bigInteger('amount');
}


var Charities = function(db, charitiesTableName, donationsTableName, transactionsTableName, donationsPercentage) {
	this.db = db;
	this.charitiesTableName = charitiesTableName;
	this.donationsTableName = donationsTableName;
	this.transactionsTableName = transactionsTableName;

	if (donationsPercentage < 0) donationsPercentage = 0;
	if (donationsPercentage > 100) donationsPercentage = 100;	

	this.DONATIONS_PERCENTAGE = donationsPercentage === undefined ? 0.2 : donationsPercentage/100;
	return this;
}

Charities.prototype._selectStatement = function() {
	var c = this.charitiesTableName;
	var d = this.donationsTableName;
	return fmt('%s.id as id, %s.name as name, ' + 
			'%s.long_description as long_description, ' + 
			'%s.short_description as short_description, ' + 
			'%s.current as current, ' +
			'%s.image_url as image_url, sum(%s.amount) as donated', c, c, c, c, c, c, d)
};

Charities.prototype.getAll = function() {
	var c = this.charitiesTableName;
	var d = this.donationsTableName;

	return this.db(c)
		.join(d, fmt('%s.id', c), '=', fmt('%s.charity_id', d), 'left')
		.select(this.db.raw(this._selectStatement()))
		.groupBy(fmt('%s.id', c))
		.then(function(charities) {
			return _.map(charities, function(charity) {
				if (charity.donated)
					charity.donated = charity.donated;
				else
					charity.donated = 0;
				return charity;
			});
		});
};

Charities.prototype.add = function(charity) {
	return this.db(this.charitiesTableName).insert({
		name: charity.name,
		short_description: charity.short_description,
		long_description: charity.long_description,
		image_url: charity.image_url,
	});
}

Charities.prototype.update = function(id, charity) {
	return this.db(this.charitiesTableName).update({
		name: charity.name,
		short_description: charity.short_description,
		long_description: charity.long_description,
		image_url: charity.image_url
	}).where('id', id);
}

Charities.prototype.setCurrent = function(id) {
	var self = this;

	return this.db(this.charitiesTableName).update({
		current: true,
	}).where('id', id).then(function(results) {
		if (results) {
			return self.db(self.charitiesTableName).update({
				current: null
			}).where('id', '<>', id);
		}
	});
}

Charities.prototype.getCurrent = function() {
	var c = this.charitiesTableName;
	var d = this.donationsTableName;

	return this.db(this.charitiesTableName)
		.join(d, fmt('%s.id', c), '=', fmt('%s.charity_id', d), 'left')
		.select(this.db.raw(this._selectStatement()))
		.where(fmt('%s.current', c), true).then(function(results) {
			if (results) return results[0];
		});
}

Charities.prototype.totalDonated = function() {
	return this.db(this.donationsTableName)
		.select(this.db.raw('sum(amount) as amount')).then(function(result) {
		var total = result[0]['amount'];
		return total;
	});
}

Charities.prototype.pendingDonations = function() {
	var self = this;

	return self.db(self.donationsTableName).orderBy('id', 'desc').limit(1)
		.then(function(results) {
		
			var spin_id_ending;
			if (results.length > 0) {
				spin_id_ending = results[0].spin_id_ending;
			}
			var q = self.db(self.transactionsTableName).where('type', 'SPIN')
			if (spin_id_ending !== undefined) {
				q = q.andWhere('spin', '>', spin_id_ending);				
			}
			return q.select(self.db.raw('max(id) as max_id, sum(dr) as dr, sum(cr) as cr'))
				.then(function(results) {
					if (results.length > 0) {
						return results[0];
					}
				});
		}).then(function(result) {
			if (result && result.dr > result.cr) {
				var lost = result.dr - result.cr;
				var donations = Math.round(lost * self.DONATIONS_PERCENTAGE);
				return donations;
			}
			return 0;
		});
}

Charities.prototype.donate = function(charityId) {
	var self = this;

	var lastSpin;
	return self.db(self.transactionsTableName).where('type', 'SPIN')
		.orderBy('id', 'desc').limit(1)
		.then(function(results) {
			if (results.length > 0) {
				lastSpin = results[0];
				return self.db(self.donationsTableName)
					.orderBy('id', 'desc').limit(1);
			}
		}).then(function(results) {
			
			var lastDonation;
			if (results.length > 0) {
				lastDonation = results[0];
			}
			if (lastSpin) {
				// there was at least one spin.
				var q = self.db(self.transactionsTableName)
					.where('type', 'SPIN')
					.andWhere('spin', '<=', lastSpin.id);
				if (lastDonation) {
					q = q.andWhere('spin', '>', lastDonation.spin_id_ending);
				}
				return q.select(self.db.raw('max(id) as max_id, sum(dr) as dr, sum(cr) as cr'))
					.then(function(results) {
						if (results.length > 0) {
							return results[0];
						}
					});
			}
		}).then(function(result) {
			if (result && result.dr > result.cr) {
				var lost = result.dr - result.cr;
				var donations = Math.round(lost * self.DONATIONS_PERCENTAGE);

				return self.db(self.donationsTableName).insert({
					spin_id_ending: result.max_id,
					charity_id: charityId,
					amount: donations,
					timestamp: new Date()
				})
			}
		}).then(function(results) {
			if (results) {
				var donated = results.length > 0;
				return donated;		
			}
			return false;
		});
};

module.exports = function(db, donationsPercentage) {
	return Promise.all([
		Utils.createTableIfNeeded(db, 'charities', charitiesTableCreator),
		Utils.createTableIfNeeded(db, 'donations', donationsTableCreator),
	]).then(function(created) {
		if (created[0]) {
			var lipsum = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';
			return insertCharity(db, 'charities', 
				'Support Julian Assange', 
				lipsum, lipsum + lipsum + lipsum, 
				'http://darkhorsenet.files.wordpress.com/2013/01/julian_assange_2010-front1.jpg');
		}
	}).then(function() {
		return new Charities(db, 'charities', 'donations', 'transactions', donationsPercentage);
	});
}