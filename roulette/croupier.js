var Promise = require('bluebird'),
	Utils = require('./utils');

var Croupier = function(casino) {
	this.casino = casino;
	this.MAX_BET = 0.1 * Math.pow(10, 8); // .1 BTC
	this.MIN_BET = 0.0001 * Math.pow(10, 8); // .001 BTC
	this.CHARITY_PERCENTAGE = 0.2;
	return this;
}

/*
	Can bet if:
	 - bet more than .001 BTC
	 - bet less than .1 BTC
*/
Croupier.prototype.userCanBetAmount = function(user, bet) {
	var self = this;
	return this.casino.getBalanceForUser(user).then(function(res) {
		var canBet = true;
		// cannot bet on 0's
		if (res.balance == 0 || bet == 0) canBet = false;

		// cannot bet more than balance
		if (bet > res.balance) canBet = false;

		// can bet only between MIN_BET and MAX_BET ...
		if (bet < self.MIN_BET || bet > self.MAX_BET) canBet = false;

		// .. the only exception is when bet is the balance and balance is less
		// than min bet
		// we made this invalid
		//if (bet == res.balance && res.balance > 0 && res.balance < this.MIN_BET) canBet = true;

		// bet cannot be negative
		if (bet <= 0) canBet = false;

		return {
			canBet: canBet,
			balance: res.balance
		};
	});
};

/**
 * A 'unit of work' method. Does the following:
 *  - reserves current spin in the transactions table
 *  - spins the roulette and gets outcome
 *  - finalizes the spin by recording the outcome in the reserved spin row
 *  - fetches updated balance for the user
 *  - returns spin outcome with the new balance
 */
Croupier.prototype.spin = Promise.method(function(roulette, user, bet, dailySeed, clientSeed) {

	var spinResult = {};
	var casino = this.casino;
	var charities = this.charities;
	var self = this;

	return casino.reserveSpin(user).then(function(spin) {
		var spinOutcome = roulette.spin(dailySeed.seed, clientSeed, user.session_id, spin.spin);
		var returned = Math.round(bet * spinOutcome.factor);

		spin.dr = bet;
		spin.cr = returned;
		spin.outcome = spinOutcome.factor;
		spin.sector = spinOutcome.sector;
		spin.wheel_id = roulette.id;
		spin.daily_seed_id = dailySeed.id;
		spin.client_seed = clientSeed;
		spin.reward = spinOutcome.reward;

		spinResult.angle = spinOutcome.angle;

		return casino.finishSpin(spin);
	}).then(function(spin) {
		spinResult.outcome = spin.outcome;
		spinResult.returned = spin.cr;
		return casino.getBalanceForUser(user);
	}).then(function(res) {
		spinResult.balance = res.balance;
		spinResult.reward = res.reward;

		return spinResult;
	});
});

/*
	Same as above, but does not record the spin.
*/
Croupier.prototype.fakeSpin = Promise.method(function(roulette, user, bet, dailySeed, clientSeed) {
	var spinResult = {};

	var spinOutcome = roulette.spin(dailySeed.seed, clientSeed, user.session_id, Utils.getRandomNumber(1000));

	var returned = Math.round(bet * spinOutcome.factor);


	return {
		angle: spinOutcome.angle,
		bet: bet,
		returned: returned,
		sector: spinOutcome.sector,
		balance: 0
	}
});

module.exports = function(casino) {
	return new Croupier(casino);
};