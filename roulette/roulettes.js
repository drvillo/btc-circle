var Utils = require('./utils')
	, _ = require('lodash')
	, Promise = require('bluebird');

var roulettes = [
	{
		id: 1,
		name: '3x',
		reward: 5,
		sectors_sets: JSON.stringify({
			'default': [3, 0.33, 1, 1, 1, 2, 0.33, 2, 0.25, 2, 0.5, 0.25, 0.5, 0.25, 1, 0.25]
		})
	},
	{
		id: 2,
		name: '5x',
		reward: 7,
		sectors_sets: JSON.stringify({
			'default': [5, 0, 0, 1, 0, 3, 0, 1, 0.5, 1.25, 0, 3, 0, 1, 0, 0]
		})
	},
	{
		id: 3,
		name: '10x',
		reward: 10,
		sectors_sets: JSON.stringify({
			'default': [10, 0, 1.25, 0, 1.25, 0, 0, 1.25, 0, 0, 0.75, 0, 1.25, 0, 0, 0]
		})
	},
];

function tableCreator(t) {
	t.increments('id').primary();
	t.string('name').unique();
	t.integer('reward').unsigned();
	t.json('sectors_sets');
}

function boostrapRoulettes(db, tableName) {
	return Promise.all(_.map(roulettes, function(roulette) {
		return db(tableName).where('name', roulette.name).then(function(results) {
			if (results.length == 0) {
				return db(tableName).insert(roulette);
			}
		}).then(function(results) {
			if (results != undefined && results.length > 0) {
				console.log('Bootstrapped ' + roulette.name + ' circle.');
			}
		}).catch(function(err) {
			console.error('Error while bootstrapping a circle: ' + err);
		});
	}));
}

var Roulette = function(data) {
	this.id = data.id;
	this.name = data.name;
	this.sectorsSets = JSON.parse(data.sectors_sets);
	this.reward = data.reward;
	return this;
}

/**
 * "Spin" the roulette. It returns the winning sector, its value (factor) and
 * and random "angle" that falls within this sector.
 */
Roulette.prototype.spin = function(dailySeed, clientSeed, sessionId, spin, sectorsSetName) {
	var sectorsSetName = sectorsSetName === undefined ? 'default' : sectorsSetName;
	var sectors = this.sectorsSets[sectorsSetName];

	if (sectors === undefined) {
		throw "Sectors set '" + sectorsSetName + "' not found for wheel '" + this.name + "'.";
	}

	// 1. Generate HMAC where:
	// message = daily seed + client seed
	// secret key = user daily session id + spin

	// console.log('DS: ' + dailySeed + ', CS: ' + clientSeed + ', SID: ' + sessionId + ', SP: ' + spin);
	var hmac = Utils.getSHA512Hmac(dailySeed + clientSeed, sessionId + spin);
	// console.log('hash: ' + hash);

	// 2. Get the first byte (2 characters) of the HMAC which is a hex string.
	// It is a random number in 0 - 255 range.
	// Normalize sector size knowing number of sectors
	// and number of possible random numbers
	// Divide random number by normalized sector size.
	// The result is random sector number.

	var randomNumber = parseInt(hmac.substring(0, 2), 16);
	var normalizedSectorSize = 256/sectors.length;
	// the floored sector number is now within range of sectors on this wheel.
	var sector = Math.floor(randomNumber/normalizedSectorSize);
	// console.log('number ' + randomNumber);

	// 3. Calculate angle by taking winning sector angle
	// and adding a random angle.
	// This is used to randomly position the hand of the wheel within
	// the sector.

	var sectorSize = 360/sectors.length;

	var extraAngle = Math.round(Utils.getRandomNumber(sectorSize));

	console.log("Sector size: %s, sector: %s, value: %s, angle: %s, extra angle: %s", sectorSize, sector, sectors[sector], sectorSize*sector, extraAngle);

	// the wheel spins clockwise, so we need to
	// get the factor from the end of the array.
	var angle = sectorSize * sector + extraAngle;
	var factor = sectors[sectors.length - 1 - sector];

	return {
		angle: angle,
		factor: factor,
		sector: sector,
		reward: this.reward
	}
}

var CirclesManager = function(db, tableName) {
	this.db = db;
	this.tableName = tableName;
	return this;
}

CirclesManager.prototype.q = function() {
	return this.db(this.tableName);
}

CirclesManager.prototype.getById = function(id) {
	return this.q().where('id', id).then(function(result) {
		return result.length > 0 ? new Roulette(result[0]) : undefined;
	});
}

module.exports = function(db) {
	return Utils.createTableIfNeeded(db, 'circles', tableCreator)
		.then(function() {
			return boostrapRoulettes(db, 'circles');
		}).then(function() {
			return new CirclesManager(db, 'circles')
		});
}
