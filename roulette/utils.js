var crypto = require('crypto')
	, base58 = require('bs58')
	, http = require('http')
	, https = require('https')
	, querystring = require('querystring')
	, Promise = require('bluebird')
	, Colors = require('colors')
	, UUID = require('node-uuid');


if (typeof String.prototype.startsWith != 'function') {
  String.prototype.startsWith = function (str){
    return this.slice(0, str.length) == str;
  };
}

module.exports.get = function(url, query) {

	var m = url.startsWith('https') ? https : http;

	var resolver = Promise.defer();
	var fullUrl = url + '?' + querystring.stringify(query);

	m.get(fullUrl, function(res) {
		var isError = res.statusCode >= 400;

		var data = '';
		res.on('data', function(chunk) {
			data += chunk;
		});

		res.on('end', function() {
			var msg;
			try {
				msg = JSON.parse(data);
			} catch(err) {
				console.log('Error parsing JSON for %s (status: %s):%s', fullUrl, res.statusCode, data);
				msg = {};
			}
			if (isError) {
				resolver.reject({
					statusCode: res.statusCode,
					message: msg
				});
			} else {
				resolver.resolve({
					statusCode: res.statusCode,
					message: msg
				});
			}
		});
	}, function(err) {
		resolver.reject(err);
	});

	return resolver.promise;
}

exports.getRandomWithHashOfPage  = function(url) {
	var resolver = Promise.defer();

	http.get(url, function(res) {
		var data = '';
		res.on('data', function(chunk) {
			data += chunk;
		});

		res.on('end', function() {
			var sha = crypto.createHash('sha256', data);
			sha.update(Math.random().toString());
			resolver.resolve(sha.digest('hex'));
		});
	}, function(error) {
		resolver.reject(error);
	});

	return resolver.promise;
}

exports.getRandomNumber = function(n) {
	return Math.random() * n;	
}

exports.getSHA512Hmac = function(message, seed) {
	var sha = crypto.createHmac('sha512', seed);
	sha.update(message);
	return sha.digest('hex')
}

exports.hashString = function(s) {
	var sha = crypto.createHash('sha256');
	sha.update(s);
	return sha.digest('hex');
} 

exports.generateSessionId = function() {
    var sha = crypto.createHash('sha256');
    sha.update(Math.random().toString());
    return sha.digest('hex');
};

exports.generateUUID = function(done) {
	var uuid = UUID.v1();
	if (done) {
		done(false, uuid);
	}
	return uuid;
};


exports.createTableIfNeeded = function(db, tableName, creator) {
	return db.schema.hasTable(tableName).then(function(exists) {
		if (!exists) {
			console.log("Table " + tableName + " does not exist. Creating");
			
			return db.schema.createTable(tableName, creator)
				.then(function(response) {
					return true;
					console.log("Table " + tableName + " created.");
				}, function(err) {
				console.log('Error creating ' + tableName + ' table: ' + err);
			});
		}
	});
}

exports.developmentErrorHandlerFactory = function(msg) {
	return function(err) {
		console.error((msg + ': ').red + err);
		console.error(err.stack);
	}
} 

exports.productionErrorHandlerFactory = function(msg) {
	return function(err) {
		console.error((msg + ': ').red + err);
		console.error(err.stack);
	}
} 


/*
	http://rosettacode.org/wiki/Bitcoin/address_validation
*/
exports.isBTCAddressValid = function(address) {
	try {
		var decodedAddress = new Buffer(base58.decode(address));
	} catch(e) {
		return false;
	}
	
	if (decodedAddress.length != 25) return false;

	// checksum check is the last 4 bytes
	var checksumCheck = decodedAddress.slice(21, 25);

	// compute double sha256 digest of the first 21 bytes

	var sha = crypto.createHash('sha256');
	sha.update(decodedAddress.slice(0, 21));
	var firstPassSHA = sha.digest();

	sha = crypto.createHash('sha256');
	sha.update(firstPassSHA);
	var secondPassSHA = sha.digest();

	var digest = secondPassSHA;

	// BTC address is valid when the first 4 bytes of the digest
	// are equal to the last 4 bytes of the base 58 decoded address

	return checksumCheck.toString('hex') == digest.slice(0, 4).toString('hex');
}