var http = require('http')
  , _ = require('lodash')
  , Url = require('url');

module.exports = function(app, url, prefix, merchantKey) {

	var processorUrl = Url.parse(url);
	var urlRegex = new RegExp('^' + prefix + '\/(.+)');
	var self=this;
	self.merchantKey=merchantKey;

	app.all(urlRegex, function(req, res, next) {
		var parsedRequest = Url.parse(req.url, true);

		var headers = _.clone(req.headers);
		headers['x-nodebit-mkey']=self.merchantKey;

		var options = {
			hostname:   processorUrl.hostname,
			port: processorUrl.port || 80,
			path:   processorUrl.pathname + req.params[0] + parsedRequest.search,
			method: req.method,
			headers: headers
		};

		var processorRequest = http.request(options, function(processorResponse) {
			processorResponse.setEncoding('utf8');

			if (processorResponse.headers['content-type'])
				res.set('Content-Type', processorResponse.headers['content-type']);
			res.writeHead(processorResponse.statusCode);
			
			processorResponse.on('data', function(chunk) {
				res.write(chunk);
			});

			processorResponse.on('end', function() {
				res.end();
			});

			processorResponse.on('close', function() {
				res.end();
			});
		});
		if (headers['content-length']) {
			processorRequest.write(req.rawBody);
		}

		processorRequest.on('error', function(e) {
  			res.json(502,{message: e});
		});

		processorRequest.end();
	});
}