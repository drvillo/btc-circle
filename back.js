var express = require('express')
var app = express()
  , server = require('http').createServer(app)
  , Knex = require('knex')
  , Utils = require('./roulette/utils')
  , U = require('util')
  , BTCProcessorProxy = require('./roulette/btc-processor-proxy')
  , Promise = require('bluebird')
  , _ = require('lodash')
  , expressValidator = require('express-validator')
  , APIClient = require('./roulette/api-client').APIClient
  , Colors = require('colors')
  , CasinoService = require('./roulette/services').CasinoService;
var users, roulettes, casino, dailySeeds, croupier, charities;

var config = require(process.env.ATALAYA_CIRCLE_CONFIG || './config');

var errorHandlerFactory = config.debug ?
	Utils.developmentErrorHandlerFactory :
	Utils.productionErrorHandlerFactory;

/*
	Application initializer
*/
function initApp(db) {
	return Promise.all([
		require('./roulette/users')(db),
		require('./roulette/roulettes')(db),
		require('./roulette/casino')(db),
		require('./roulette/seeds')(db),
		require('./roulette/charities')(db)
	]).then(function(results) {
		users = results[0];
		roulettes = results[1];
		casino = results[2];
		dailySeeds = results[3];
		charities = results[4];
		croupier = require('./roulette/croupier')(casino);
		casinoService = new CasinoService(casino, users);

		server.listen(config.backendPort);
		console.info(('Backend started on port ' + config.backendPort).green);
		return app;
	}).catch(errorHandlerFactory('Error while initializing'));
}

/*
	App initialization entry point for unit tests.
*/
module.exports = initApp;

/*
	MAIN. Called when the module is run directly by node.
*/
if (require.main === module) {
	var db = Knex.initialize(config.db);
	module.exports(db);
}


/*
	Express JS configuration.
*/

/*
	if request path starts with '/btc', it's a proxy request.
	we should not use bodyParser, because we loose the raw body
	that is needed to be sent on to the processor.
*/
var bp = express.bodyParser();
app.use (function(req, res, next) {
	if (req.path.slice(0, 5) =='/btc/') {
	    var data = new Buffer(0);
	    req.on('data', function(chunk) {
	       data = Buffer.concat([data, chunk]);
	    });
	    req.on('end', function() {
	        req.rawBody = data;
	        next();
	    });

	} else {
		bp(req, res, next);
	}
});
app.use(expressValidator([]));
app.use(app.router);
app.use('/js', express.static(__dirname + '/frontend/js'));
app.use('/img', express.static(__dirname + '/frontend/img'));
app.use('/css', express.static(__dirname + '/frontend/css'));
app.use('/less', express.static(__dirname + '/frontend/less'));
app.use('/fonts', express.static(__dirname + '/frontend/fonts'));

/*
	Endpoints:
*/

/*
	Get backend UI.
*/
app.get('/', function (req, res) {
  res.sendfile(__dirname + '/frontend/back.html');
});

app.get(/^\/partials\/(.+)/, function (req, res) {
  res.sendfile(__dirname + '/frontend/partials/' + req.params[0]);
});


/*
	Set up BTC Processor proxy and API client.
*/
var btcProcessorUrl = U.format('http://%s:%s', config.btcApiHost,
	config.btcApiPort);
BTCProcessorProxy(app, btcProcessorUrl, '/btc',config.merchantKey)
var processorClient = new APIClient(btcProcessorUrl,config.merchantKey);

/*
	Get a list of all users in the system.
*/
app.get('/users', function(req, res) {

	var method = users.getAll;

	if (req.query.filter === 'active') {
		method = users.getActive;
	} else if (req.query.filter === 'inactive') {
		method = users.getInactive;
	}

	method.apply(users, [req.query.limit, req.query.offset]).then(function(users) {
		res.json(users);
	}).catch(function(err) {
		errorHandlerFactory('Error getting users')(err);
		res.send(500, 'Error getting users');
	});
});

app.get('/users/stats', function(req, res) {
	Promise.join(
		users.getActiveCount(),
		users.getInactiveCount()
	).spread(function(active, inactive) {
		res.json({
			active: active,
			inactive: inactive,
			total: active + inactive
		});
	}).catch(function(err) {
		errorHandlerFactory('Error getting users stats')(err);
		res.send(500, 'Error getting users stats');
	});
});

/*
	Return transactions filtered by criteria.

	If `term` is in query, all other criteria are omitted.
*/
app.get('/transactions', function(req, res) {

	if (req.query.limit)
		req.assert('limit', 'Invalid number').isInt();

	if (req.query.offset)
		req.assert('offset', 'Invalid number').isInt();

	if (req.query.term) {

		var errors = req.validationErrors();
		if (errors) {
			return res.json(400, errors);
		}

		casino.searchTransactionsByTerm(req.query.term, req.query.limit, req.query.offset)
			.then(function(transactions) {
				res.json(transactions);
			}).catch(function(err) {
				errorHandlerFactory('Error getting transactions')(err);
				res.send(500, 'Error getting transactions');
			});
	} else {
		if (req.query.fromTimestamp) {
			req.assert('fromTimestamp', 'Invalid format').isDate();
			req.sanitize('fromTimestamp').toDate();
		}

		if (req.query.toTimestamp) {
			req.assert('toTimestamp', 'Invalid format').isDate();
			req.sanitize('toTimestamp').toDate();
		}

		if (req.query.status) {
			if (typeof req.query.status === 'string')
				req.assert('status', 'Unknown status').isIn(casino.Statuses);
			else {
				_.each(req.query.status, function(status, idx) {
					req.assert('status.' + idx, 'Unknown status').isIn(casino.Statuses);
				});
			}
		}

		var errors = req.validationErrors();
		if (errors) {
			return res.json(400, errors);
		}

		casino.searchTransactions(req.query)
			.then(function(transactions) {
				res.json(transactions);
			}).catch(function(err) {
				errorHandlerFactory('Error getting transactions')(err);
				res.send(500, 'Error getting transactions');
			});
	}
});

app.put('/transactions/:id/status', function(req, res) {
	req.assert('status').isIn(casino.Statuses);
	var errors = req.validationErrors();
	if (errors) {
		return res.json(400, errors);
	}

	casino.setTransactionStatus(req.params.id, req.body.status)
		.then(function(wasSet) {
			if (!wasSet) {
				res.json(404, {message: "Not Found"});
			} else {
				res.json(200, {status: req.body.status});
			}
		}).catch(function(err) {
			errorHandlerFactory('Error updating transaction status')(err);
			res.send(500, 'Error updating transaction status');
		});
});

/*
	Get total donations.
*/
app.get('/profit', function(req, res) {
	casino.getProfit().then(function(total) {
		res.json({amount: total});
	}).catch(function(err) {
		errorHandlerFactory('Error getting profit')(err);
		res.send(500, 'Error getting profit');
	});
});

/*
	Get total bonus granted.
*/
app.get('/bonuses', function(req, res) {
	casino.getTotalBonusGranted().then(function(total) {
		res.json({amount: total});
	}).catch(function(err) {
		errorHandlerFactory('Error getting bonuses')(err);
		res.send(500, 'Error getting bonuses');
	});
});

/*
	Get total donations.
*/
app.get('/balance/:exclude', function(req, res) {
	casino.getTotalBalance(req.param.exclude).then(function(total) {
		res.json({amount: total});
	}).catch(function(err) {
		errorHandlerFactory('Error getting profit')(err);
		res.send(500, 'Error getting profit');
	});
});


/*
	Get a list of all charities with total donations.
*/
app.get('/charities', function(req, res) {
	charities.getAll().then(function(charities) {
		res.json(charities);
	}).catch(function(err) {
		errorHandlerFactory('Error getting charities')(err);
		res.send(500, 'Error getting charities');
	});
});

app.post('/charities', function(req, res) {
	req.checkBody('name', 'Missing name').notEmpty();
	req.checkBody('short_description', 'Missing').notEmpty();
	req.checkBody('long_description', 'Missing').notEmpty();
	req.checkBody('image_url', 'Missing').notEmpty();
	req.checkBody('current', 'Should not be set').isNull();

	var errors = req.validationErrors(true);
	if (errors) {
		return res.json(400, errors);
	}

	charities.add(req.body).then(function() {
		res.json(201, {});
	}).catch(function(err) {
		errorHandlerFactory('Error adding charities')(err);
		res.send(500, 'Error adding charities');
	});
});

app.put('/charities/:id', function(req, res) {
	req.checkBody('name', 'Missing name').notEmpty();
	req.checkBody('short_description', 'Missing').notEmpty();
	req.checkBody('long_description', 'Missing').notEmpty();
	req.checkBody('image_url', 'Missing').notEmpty();
	req.sanitize('current').isNull();

	var errors = req.validationErrors(true);
	if (errors) {
		return res.json(400, errors);
	}

	charities.update(req.params.id, req.body).then(function() {
		res.json(200, {});
	}).catch(function(err) {
		errorHandlerFactory('Error adding charities')(err);
		res.send(500, 'Error adding charities');
	});
});

app.post('/charities/:id/current', function(req, res) {
	charities.setCurrent(req.params.id).then(function() {
		res.json(200, {});
	}).catch(function(err) {
		errorHandlerFactory('Error adding charities')(err);
		res.send(500, 'Error adding charities');
	});
});

/*
	Get pending donations.
*/
app.get('/donations/pending', function(req, res) {
	charities.pendingDonations().then(function(total) {
		res.json({amount: total});
	}).catch(function(err) {
		errorHandlerFactory('Error getting pending donations')(err);
		res.send(500, 'Error getting pending donations');
	});
});

/*
	Get total donations.
*/
app.get('/donations/total', function(req, res) {
	charities.totalDonated().then(function(total) {
		res.json({amount: total});
	}).catch(function(err) {
		errorHandlerFactory('Error getting total donations')(err);
		res.send(500, 'Error getting total donations');
	});
});


app.post('/charities/:id/donations', function(req, res) {
	charities.donate(req.params.id).then(function(donated) {
		res.json({donated:donated});
	}).catch(function(err) {
		errorHandlerFactory('Error getting charities')(err);
		res.send(500, 'Error getting charities');
	});
});

/*
	Query params:

	* requireAttention - return wallets wih positive balance in the processor
		and "0" in the wallet. When set, "offset" and "limit" are omitted.
	* limit
	* offset


	Returns a list of objects:
	{
		id: 123, // processor wallet record ID
		address: 'xyz', // front wallet ID
		balanceNanoConfirmed: 123, // ???
		balanceNanoEstimated: 123, // ???
		user: {
			id: 123, // game user ID
			name: foobar, // game user name
			balance: 123, // game balance
		},
		lastTransaction: {
			id: 123, // transaction ID
			type: 'WITHDRAWAL',
			destination: 123zxcv, // transaction destination
			amount: 123, // transaction amount
			status: 'FAILED', // status
		}
	}
*/
app.get('/front_wallets', function(req, res) {

	// validate/sanitize query params
	req.sanitize('requireAttention').toBoolean();

	var processorQuery = {
		offset: req.query.offset,
		limit: req.query.limit
	};

	if (req.query.requireAttention) {
		processorQuery.offset = 0;
		processorQuery.limit = 1000000;
		processorQuery.positiveBalanceOnly = true;
	}

	if (req.query.address) {
		processorQuery.address = req.query.address;
	}

	// 1. get front wallets from the processor.
	processorClient.get('/rest/private/front_wallets', processorQuery)
		.then(function(response) {
			if (!response.ok) return res.json(response.status, response.body);

			// 2. get user statuses by front wallets
			var frontWalletsIds = _.map(response.body, function(w) {
				return w.address;
			});

			return Promise.join(
				users.getByFrontWalletIds(frontWalletsIds),
				users.getLastTransactionsByFrontWalletIds(frontWalletsIds)
			).spread(function(totals, lastTransactions) {
				totals = _.indexBy(totals, 'front_wallet_id');
				lastTransactions = _.indexBy(lastTransactions, 'front_wallet_id');

				var wallets = _.map(response.body, function(w) {
					var t = totals[w.address];
					var l = lastTransactions[w.address];

					w.user = {};
					w.lastTransaction = {};

					if (t) {
						w.user.id = t.user_id;
						w.user.name = t.username;
						w.user.balance = (t.cr - t.dr) || 0;
					}

					if (l) {
						w.lastTransaction.id = l.transaction_id;
						w.lastTransaction.type = l.transfer_type;
						w.lastTransaction.destination = l.destination;
						w.lastTransaction.amount = l.cr || l.dr;
						w.lastTransaction.status = l.status;
					}

					return w;
				});

				if (req.query.requireAttention) {
					// remove all without users having zero balance
					wallets = _.filter(wallets, function(w) {
						return w.user.balance == 0;
					});
				}

				res.json(200, wallets);
			});
		}).catch(function(err) {
			errorHandlerFactory('Error getting wallets')(err);
			res.send(500, 'Error getting wallets');
		});
});


/*
	Deposit amount to front wallet.

	This endpoint is called by the BTC Processor.
*/
app.post('/bitcoin/deposit', function(req, res) {
	var fields = ['amount', 'walletId', 'transactionId'];

	var missingFields = _.filter(fields, function(field) {
		return req.body[field] === undefined;
	})

	if (missingFields.length > 0 ) {
		res.json(400, {message: 'Missing fields: ' + missingFields.join(', ')});
		return;
	}

	var amount = parseFloat(req.body.amount);

	if (isNaN(amount) || amount <= 0)
		return res.json(400, {message: 'Amount must be a positive number'});


	users.getByWalletId(req.body.walletId).then(function(user) {
		if (user === undefined) {
			res.json(404, {message: 'No such wallet'});
		} else {
			if (req.body.amount <= 0) {
				res.json(400, {message: 'Amount must be positive'});
			} else {
				return casino.transferMoney(
					user,
					req.body.amount,
					req.body.transactionId,
					req.body.walletId)
					.then(function() {
						res.json(202, {message: 'amount accepted as not confirmed'});
					});
			}
		}
	}).catch(function(err) {
		errorHandlerFactory('Error while depositing')(err);
		res.json(500, {message: 'Could not deposit'});
	});
});

/*
	Confirm previous deposit or withdrawal.

	This endpoint is called by the BTC Processor.
*/
app.post('/bitcoin/confirm', function(req, res) {
	if (req.body.transactionId === undefined) {
		res.json(400, {message: 'Transaction ID missing'});
		return;
	}
	casino.confirmTransfer(req.body.transactionId).then(function(done) {
		if (done) {
			res.json(201, {message: "amount confirmed"});
		} else {
			res.json(404, {message: 'No such transaction'});
		}
	}).catch(function(err) {
		errorHandlerFactory('Error confirming transaction')(err);
		res.json(500, {message: 'Could not confirm'});
	});
});


/*
	Invalidate previous deposit or withdrawal.

	This endpoint is called by the BTC Processor.
*/
app.post('/bitcoin/invalidate', function(req, res) {
	if (req.body.transactionId === undefined) {
		res.json(400, {message: 'Transaction ID missing'});
		return;
	}
	casino.failTransfer(req.body.transactionId).then(function(done) {
		if (done) {
			res.json(201, {message: "amount invalidated"});
		} else {
			res.json(404, {message: 'No such transaction'});
		}
	}).catch(function(err) {
		errorHandlerFactory('Error invalidating transaction')(err);
		res.json(500, {message: 'Could not invalidate'});
	});
});

/*

	This endpoint is called by the BTC Processor.
*/
app.get('/users/:id/totaldeposited', function(req, res) {
	if (req.params.id === undefined) {
		res.json(400, {message: 'User ID missing'});
		return;
	}
	casino.getTotalDepositedByUser(req.params.id).then(function(total) {
		console.log(total);
		if (total) {
			res.json(200, {amount: total});
		} else {
			res.json(404, {message: 'No such user'});
		}
	}).catch(function(err) {
		errorHandlerFactory('Error fetching user info')(err);
		res.json(500, {message: 'Error fetching user info'});
	});
});

/*

	This endpoint is called by the BTC Processor.
*/
app.get('/users/:id/totalwithdrawn', function(req, res) {
	if (req.params.id === undefined) {
		res.json(400, {message: 'User ID missing'});
		return;
	}
	casino.getTotalWithDrawnByUser(req.params.id).then(function(total) {
		console.log(total);
		if (total) {
			res.json(200, {amount: total});
		} else {
			res.json(404, {message: 'No such user'});
		}
	}).catch(function(err) {
		errorHandlerFactory('Error fetching user info')(err);
		res.json(500, {message: 'Error fetching user info'});

	});
});

/*
	award a bonus to a user:
	{
		"userId": 1,
		"bonus": 1000, // nanocoins
	}
*/
app.post('/bonuses', function(req, res) {
	if (!req.body.userId || !req.body.bonus) return res.json(400, {message: 'Malformed request'});

	casino.awardBonus(req.body.userId, parseFloat(req.body.bonus)).then(function(result) {
		res.json(result);
	});
})

/**
	Returns running vs accumulated balances for every user.
	Used temporarily to check that Roman did not screw up the
	transaction code that updates balances on the user records.
*/
app.get('/balances', function(req, res) {

	var balances = [];

	users.getAll().then(function(uu) {

		if (uu.length == 0) {
			return res.json([]);
		}

		return Promise.all(_.each(uu, function(user) {
			casino.getBalanceForUser(user).then(function(a) {
				balances.push({
					id: user.id,
					runningBalance: a.balance,
					runningRewards: a.reward,
					balance: user.confirmed_balance + user.unconfirmed_balance,
					rewards: user.rewards
				});

				if (balances.length == uu.length) {
					res.json(balances);
				}
			});
		}));
	}).catch(function(err) {
		errorHandlerFactory('Error ')(err);
		res.json(500, {message: 'Error'});
	});
});
