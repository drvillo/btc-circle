	var express = require('express')
var app = express()
  , server = require('http').createServer(app)
  , url = require('url')
  , io = require('socket.io').listen(server)
  , passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy
  , Knex = require('knex')
  , _ = require('lodash')
  , moment = require('moment')
  , utils = require('./roulette/utils')
  , U = require('util')
  , Promise = require('bluebird')
  , Colors = require('colors')
  , QREncoder = require('qr').Encoder
  , BTCProcessorClient = require('./roulette/btc')
  , Assets = require('./assets')
  , consolidate = require('consolidate')
  , PlayersPool = require('./roulette/players-pool').MemoryPool
  , CasinoService = require('./roulette/services').CasinoService
  , git = require('git-rev');
var users, roulettes, casino, dailySeeds, croupier, charities,
	casinoService, players;

var CurrentGitHash;
git.short(function (str) {
  CurrentGitHash = str;
})

var NANO_IN_BTC = Math.pow(10, 8);

/*
	Template engine
*/
app.engine('html', consolidate.ejs);
app.set('view engine', 'html');
app.set('views', __dirname + '/frontend');

var config = require(process.env.ATALAYA_CIRCLE_CONFIG || './config');

var errorHandlerFactory = config.debug ?
	utils.developmentErrorHandlerFactory :
	utils.productionErrorHandlerFactory;

/*
	Cookie parser.
*/
var parseCookie = express.cookieParser(config.sessionCookieSecret);

/*
	Bitcoin processor client.
*/

var getNewBTCClient = function(hostname) {
	return BTCProcessorClient(U.format('http://%s:%s',
		hostname || config.btcApiHost,
		config.btcApiPort
	),config.merchantKey);
}
var btcClient = getNewBTCClient();


/*
	Application initializer.
*/
function initApp(db) {
	return Promise.join(
		require('./roulette/users')(db),
		require('./roulette/roulettes')(db),
		require('./roulette/casino')(db),
		require('./roulette/seeds')(db, !config.production),
		require('./roulette/charities')(db, config.donationsPercentage)
	).spread(function(_users, _roulettes, _casino, _dailySeeds, _charities) {
		users = _users;
		roulettes = _roulettes;
		casino = _casino;
		dailySeeds = _dailySeeds;
		charities = _charities;
		croupier = require('./roulette/croupier')(casino);
		casinoService = new CasinoService(casino, users);
		players = new PlayersPool({
			userDetailsFactory: casinoService.getUserDetails.bind(casinoService),
			errorHandler: errorHandlerFactory()
		});

		app.locals({
			users: users,
			roulettes: roulettes,
			casino: casino,
			dailySeeds: dailySeeds,
			charities: charities,
			croupier: croupier,
			casinoService: casinoService
		});

		console.info('Game DB initialized'.green);
		return checkDailySeedExists();
	}).then(function() {
		console.info('Daily seed checked'.green);
		init();
		server.listen(config.port);
		console.info(U.format('Game started on port %s', config.port).green);
		return app;
	}).catch(errorHandlerFactory('Error while initializing'));
}

/*
	App initialization entry point for unit tests.
*/
module.exports = initApp;

/*
	MAIN
*/
if (require.main === module) {
	var db = Knex.initialize(config.db);
	module.exports(db);
}

/*
	Exceptions.
*/
function PreconditionError(message) { this.message = message; }
PreconditionError.prototype = Object.create(Error.prototype);


/*
	Emits new daily seed to players via websockets.
*/
function announceDailySeedHash(socket) {
	var emitter = socket === undefined ? io.sockets : socket;
	dailySeeds.getCurrentSeed().then(function(seed) {
		emitter.emit("dailySeedHash", seed.hash);
	}).catch(errorHandlerFactory('Error announcing daily seed hash'));
}

var spinPresenter = function(spin) {
	return {
		user_id: spin.user_id,
		timestamp: spin.timestamp,
		bet: spin.dr,
		returned: spin.cr,
		outcome: spin.outcome,
		wheel_id: spin.wheel_id,
		sector: spin.sector,
		client_seed: spin.client_seed,
		spin_id: spin.session_id + spin.spin,
		username: spin.name ? spin.name : ''
	}
}

/*
	Passport config.
*/
passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	users.get(userId).then(function(user) {
		if (!user) {
			done('No user found');
		} else {
			done(null, user);
		}
	});
});

passport.use(new LocalStrategy(function(username, password, done) {
	var u;
	users.getByName(username).then(function(user) {
		u = user;
		if (!user) {
			return done(null, false, {message: 'Unknown user' + username});
		} else {
			return users.verifyPassword(user, password);
		}
	}).then(function(passwordValid) {
		if (passwordValid) {
			done(null, u);
		} else {
			done(null, false, {message: 'Invalid password'});
		}
	}).catch(function(err) {
		return done(err);
	});
}));


/*
 Express JS endpoints:
*/
app.configure(function() {
	io.set('log level', 2);

	app.use(parseCookie);
	app.use(express.cookieSession({
		key: 'sid'
	}));
	app.use(passport.initialize());

	// custom logger
	app.use(function(req, res, next) {
		function logRequest() {

			var msg = '[' + new Date().toUTCString() + '] ' + res.statusCode + " UID: " + req.session.userId
			+ ' (' + req.ip + ') ' + req.method + ' ' + req.path + ' ' + (req.query ? JSON.stringify(req.query) : '');
			if(req.body.password!=null){
				maskedbody=req.body;
				maskedbody.password="****";
				msg += ' ↓ ' + JSON.stringify(maskedbody) + ' ↑ ' + JSON.stringify(res._body);
			}else{
				msg += ' ↓ ' + JSON.stringify(req.body) + ' ↑ ' + JSON.stringify(res._body);
			}
			console.log(msg);
		};

		// logs only json events
		var old = res.json;
		res.json = function() {
			old.apply(res, arguments);
			if (arguments.length > 1)
				res._body = arguments[1];
			else
				res._body = arguments[0];

			logRequest();
		}

		// res.on('finish', logRequest);
		// res.on('close', logRequest);
		next();
	});

	app.use(express.bodyParser());
	app.use(app.router);



	app.use('/js', express.static(__dirname + '/frontend/js'));
	app.use('/img', express.static(__dirname + '/frontend/img/' + config.gameId));
	app.use('/css', express.static(__dirname + '/frontend/css'));
	app.use('/fonts', express.static(__dirname + '/frontend/fonts'));

	// Less is compiled to CSS in production. Hiding less files.
	if (!config.production) {
	  app.use('/less', express.static(__dirname + '/frontend/less'));
	}

});

app.get('/', function (req, res) {
	var sendPage = function() {
		if (req.query.secret) {
			res.redirect(url.parse(req.url).pathname);
		} else {
			res.render('index', {
				production: config.production,
				gameTitle: config.gameTitle,
				gameTagline: config.gameTagline,
				gameId: config.gameId,
				gameDomain: config.gameDomain,
				gameDonationsDisabled: config.gameDonationsDisabled,
				jsFiles: Assets.GameJSFiles,
				'static': config.production ? config.staticAssetsLocation : '',
				version: CurrentGitHash
			});
		}
	}

	// 1. get user by secret
	return users.getBySecret(req.query.secret).then(function(user) {
		if (user) {
			req.session.userId = user.id;
			sendPage();
			return true;
		}
		return false;
	}).then(function(pageSent) {
		if (!pageSent) {
			// 2. if not found - get user by session
			return users.get(req.session.userId).then(function(user) {
				if (user) {
					req.session.userId = user.id;
					sendPage();
					return true;
				}
				return false;
			});
		}
		return true;
	}).then(function(pageSent) {
		// 3. if not found - create user
		if (!pageSent) {
			delete req.session.userId;
			return users.create().then(function(user) {
				req.session.userId = user.id;
				sendPage();
			});
		}
	}).catch(function(err) {
		errorHandlerFactory('Error getting or creating new user')(err);
		res.send(500, 'Something went wrong');
	});

});

app.get('/partials/:part', function (req, res) {
  // res.sendfile(__dirname + '/frontend/partials/' + req.param('part'));

	res.render('partials/' + req.param('part'), {
		production: config.production,
		gameTitle: config.gameTitle,
		gameTagline: config.gameTagline,
		gameId: config.gameId,
		gameDomain: config.gameDomain,
		gameDonationsDisabled: config.gameDonationsDisabled,
		'static': config.production ? config.staticAssetsLocation : '',
		version: CurrentGitHash
	});
});

app.get('/qrcode', function(req, res) {
	var text = req.query.text;

	if (!text) {
		return res.json(404, {message: 'No text provided.'});
	}

	var encoder = new QREncoder;
	encoder.on('end', function(data){
		res.set('Content-Type', 'image/png');
		res.write(data);
		res.end();
	});
	encoder.encode(text);
});

/*
	Update personal wallet and get front wallet in return (if does not exist yet).
*/
app.put('/personal_wallet', function(req, res) {
	var personalWalletId = req.body.wallet_id;

	if (!req.session.userId) {
		return res.json(401, {message: 'Not authorized'});
	}
	if (!personalWalletId) {
		return res.json(400, {message: 'wallet_id not provided'});
	}

	if (!Utils.isBTCAddressValid(personalWalletId)) {
		return res.json(400, {message: 'Incorrect address'});
	}

	var theUser;
	users.updatePersonalWallet(req.session.userId, personalWalletId)
		.then(function(user) {
			theUser = user;
			if (user) {
				if (!user.wallet_id) {
					return btcClient.getNewWalletId();
				}
			}
		}).then(function(wallet) {
			if (wallet) {
				return users.updateFrontWallet(req.session.userId, wallet.address, wallet.host);
			}
		}).then(function(user) {
			if (!theUser) {
				res.json(404, {message: 'No such user'});
			} else {
				var u = user || theUser; // get the most recent updated user.
				res.json({
					id: u.id,
					name: u.name,
					wallet: u.wallet_id
				})
				players.announceDetails(u.id);
			}
		}).catch(function(err) {
			errorHandlerFactory('Error updating wallets')(err);
			res.send(500, 'Something went wrong');
		});
});

app.post('/login', function(req, res, next) {
	passport.authenticate('local', {
		successRedirect: undefined,
		failureRedirect: undefined,
		successReturnToOrRedirect: undefined
	}, function(err, user, info) {

	if (err) {
		console.err(err);
		return res.json(400, {
			message: "invalid details"
		});
	}

	if (!user) {
		console.log(user);
		return res.json(401, {
			message: 'No such user',
		});
	}
	req.logIn(user, function(err) {
		if (err) {
			console.err(err);
			return res.json(400, {
				message: "invalid details"
			});
		}

		var oldId = req.session.userId;
		req.session.userId = user.id;

		var socket = players.getSocketForPlayer(oldId);
		players.removePlayer(oldId);
		players.addPlayer(user.id, socket);
		players.announceDetails(user.id);

		res.json({
			id: user.id,
			name: user.name
		});
	});
	})(req, res, next);
});

app.post('/signup', function(req, res, next) {
	var userId = req.session.userId;
	var username = req.body.username;
	var password = req.body.password;

	if (!username) { return res.json(400, {message: 'username parameter required'});}
	if (!password) { return res.json(400, {message: 'password parameter required'});}
	if (password.length < 3) { return res.json(400, {message: 'password must be at least 3 characters long'});}

	var user;

	users.getByName(username).then(function(user) {
		if (!user) {
			return users.get(userId);
		} else {
			throw 'name_exists';
		}
	})
	.then(function(u) {
		if (u) {
			user = u;
			return users.signUp(user, username, password);
		} else {
			throw "No user found."
		}
	}).then(function() {
		return users.get(user.id);
	}).then(function(user) {
		res.json({
			id: user.id,
			name: user.name
		});
	}).catch(function(err) {
		if (err === 'name_exists') {
			res.json(409, {message: 'This username is already taken'});
		} else {
			errorHandlerFactory('Error while signing user up')(err);
			res.send(500, 'Something went wrong');
		}
	});
});

app.post('/logout', function(req, res) {
	var socket = players.getSocketForPlayer(req.session.userId);
	if (socket) socket.disconnect();
	players.removePlayer(req.session.userId);
	req.session.userId = undefined;

	res.json({ok: true});
});

app.post('/spin', function(req, res) {
	var userId = req.session.userId;
	var bet = parseInt(req.body.bet);
	var rouletteId = req.body.circle_id || 1;
	var clientSeed = req.body.client_seed;

	if (clientSeed === undefined) {
		res.send(400, 'No client seed provided');
		return;
	}

	if (bet === undefined) {
		res.send(400, 'No bet provided.');
		return;
	}

	var spin = function(user) {
		var fakeSpin = !user.wallet_id;
		var msg = {};

		croupier.userCanBetAmount(user, bet).then(function(result) {
			if (!result.canBet) {
				if (result.balance == 0) {
					fakeSpin = true;
				} else {
					throw new PreconditionError('Cannot bet this amount. Your bet must be between 0.1 and 0.0001 BTC');
				}
			}

			return Promise.join(
				roulettes.getById(rouletteId),
				dailySeeds.getCurrentSeed()
			);
		}).spread(function(roulette, dailySeed) {
			if (!roulette) throw new PreconditionError('No cicle with ID ' + rouletteId + ' found.');
			if (!dailySeed) throw new PreconditionError('No daily seed found');

			if (fakeSpin) {
				return croupier.fakeSpin(roulette, user, bet, dailySeed, clientSeed);
			} else {
				return croupier.spin(roulette, user, bet, dailySeed, clientSeed);
			}
		}).then(function(result) {
			msg = {
				'sector': result.sector,
				'returned': result.returned,
				'bet': bet,
				'balance': result.balance || 0,
				'reward': result.reward,
				'angle': result.angle,
			};
			if (fakeSpin) {
				msg.dummy = true;
			}
			return Promise.join(
				casino.getTotalSpinsForUser(user.id),
				casino.getBalanceForUser(user, true)
			);
		}).spread(function(spins, confirmedBalance) {

			msg.confirmedBalance = confirmedBalance.balance ? confirmedBalance.balance : 0;
			msg.unconfirmedBalance = msg.balance - msg.confirmedBalance;

			msg.totalSpins = spins;
			res.json(msg);
		}).catch(PreconditionError, function(error) {
			console.log('Error: ' + error.message);
			res.json(412, {message: error.message});
		}).catch(function(error) {
			errorHandlerFactory('Error while spinning')(error);
			res.send(500, 'Something went wrong');
		});
	}
	users.get(userId).then(function(user) {
		if (user === undefined) {
			res.send(404, 'Invalid user ID');
		} else {
			spin(user);
		}
	}, function(err) {
		errorHandlerFactory('Error getting user')(err);
		res.send(500, 'Could not get user');
	});

});

app.post('/withdraw', function(req, res) {
	var userId = req.session.userId;
	var user;
	var destination_id = req.body.destination_wallet_id;
	var withdrawalSum = req.body.amount;

	if (!destination_id)
		return res.json(400, {message: 'No Destination ID provided'});

	users.get(userId).then(function(u) {
		user = u
		if (user === undefined) throw 'User not found';
		// get confirmed balance for user
		return Promise.join(
			casino.getBalanceForUser(user, true),
			casino.getLastTransactionForUser(user.id),
			casino.getTotalBetsSinceLastBonus(user.id)
		);
	}).spread(function(result, lastTransaction, betsSinceBonus) {

		// if user's last transaction was a deposit,
		// we won't allow the user to withdraw until he
		// spins the wheel at least once
		if (!lastTransaction || lastTransaction.transfer_type == 'DEPOSIT') {
			res.json(412, {message: "Sorry, you can't withdraw without playing"})
			return null;
		}

		// if the player received a bonus, he is not allowed to withdraw till
		// he puts a total sum of bets that equal the sum of the bonus
		if (betsSinceBonus && betsSinceBonus.bets < betsSinceBonus.bonus) {
			var msg = U.format("You recently got a bonus and must bet at least %s BTC in total to be able to withdraw",
				(betsSinceBonus.bonus - betsSinceBonus.bets)/NANO_IN_BTC);
			return res.json(412, {message: msg});
		}

		if (withdrawalSum === undefined) {
			withdrawalSum = result.balance;
		}
		if (result.balance > 0 && withdrawalSum <= result.balance) {
			var transactionId = utils.generateUUID();
			var client = getNewBTCClient(user.wallet_hostname);
			return client.withdraw(withdrawalSum, user.wallet_id, destination_id, transactionId);
		} else {
			if (result.balance == 0) {
				res.json(412, {message: 'Balance is 0'});
			} else {
				res.json(412, {message: 'Cannot withdraw more than balance'});
			}
		}
	}).then(function(transactionId) {
		if (withdrawalSum && transactionId) {
			return casino.transferMoney(user, -withdrawalSum, transactionId, destination_id);
		}
	}).then(function(result) {
		if (result) {
			if (result.balance !== undefined) {
				res.json({balance: result.balance});
			} else {
				res.json({});
			}
		}
	})
	.catch(function(err) {
		errorHandlerFactory('Error while doing withdrawal')(err);
		res.json(500, {message: 'Could not withdraw'});
	});
});

app.get('/spins', function(req, res) {
	if (!req.query.user_id) return res.json(400, {message: 'User ID is missing'});
	if (!req.query.date) return res.json(400, {message: 'Date is missing'});

	casino.findSpins(req.query.user_id, req.query.date).then(function(spins) {
		res.json(_.map(spins, spinPresenter));
	}).catch(function(err) {
		errorHandlerFactory('Error while searching for spins')(err);
		res.json(500, {message: 'Error'});
	});
});


app.get('/seed', function(req, res) {
	if (!req.query.timestamp) return res.json(400, {message: '"timestamp" parameter missing'});

	var timestamp = moment(req.query.timestamp);

	if (timestamp.isSame(moment(), 'day')) {
		return res.json(404, {message: "Today's seed will be available tomorrow"})
	}

	dailySeeds.findForTimestamp(timestamp).then(function(seed) {
		if (seed) {
			res.json({seed: seed.seed});
		} else {
			res.json(404, {message: "Seed could not be found"});
		}
	}).catch(function(err) {
		errorHandlerFactory('Error while searching a seed')(err);
		res.json(500, {message: 'Could not find seed'});
	});
});

function init() {
	io.set('authorization', function(handshake, callback) {
		  if (handshake.headers.cookie) {
		    parseCookie(handshake, null, function(err) {
		    	if (err) {
		    		console.error('Error: %s', err);
		    	}
		    	var session = handshake.signedCookies['sid'];
		    	if (session && session.userId) {
				  	return callback(null, true);
		    	} else {
				  	return callback('No user session found', false);
		    	}
		    });
		  } else {
		  	return callback('Session required', false);
		  }
	});

	// 1. when a new user is connected, send recent stats and
	// daily seed hash
	io.sockets.on('connection', function (socket) {

	    parseCookie(socket.handshake, null, function(err) {
	    	var session = socket.handshake.signedCookies['sid'];
	    	players.addPlayer(session.userId, socket);
	    	players.announceDetails(session.userId)
	    	console.log('User ' + session.userId + ' connected');
	    });

		var limit = 30;
		var message = {};

		Promise.join(
			casino.getLatestResults(limit),
			casino.getLeaders(10),
			casino.getWeeklyLeaders(10),
			charities.totalDonated(),
			charities.getCurrent()
		).spread(function(results, leaders, wleaders, totalDonated, currentCharity) {
			message.leaders = leaders;
			message.wleaders  = wleaders;
			message.newResults = _.map(results, spinPresenter);
			message.totalDonated = totalDonated;
			message.currentCharity = currentCharity;

			return casino.getTotalSpins();
		}).then(function(total) {
			message.totalSpins = total;
			socket.emit('recentStats', message);
			announceDailySeedHash(socket);
		}).catch(errorHandlerFactory('Error getting latest results'));

		socket.on('disconnect', function() {
		    parseCookie(socket.handshake, null, function(err) {
		    	var session = socket.handshake.signedCookies['sid'];
		    	players.removePlayer(session.userId);
		    	console.log('User ' + session.userId + ' disconnected');
		    });
		})
	});

	// 2. Every X seconds publish new stats to all users
	// and publish new transfers.
	var lastTimestamp = new Date();
	var lastTimestampUsers = new Date();

	var lastCheckedDailySeed = null;

	setInterval(function() {

		var message = {};

		// 1. announce new transfers
		casino.getUsersWithTransfersSince(lastTimestampUsers).then(function(userIds) {
			lastTimestampUsers = new Date();
			_.map(userIds, function(id) {
				players.announceDetails(id);
			});
		}).catch(errorHandlerFactory('Error getting users with new transfers'));

		// 2. announce new spin results
		Promise.join(
			casino.getResultsSince(lastTimestamp),
			casino.getLeaders(10),
			casino.getWeeklyLeaders(10),
			charities.totalDonated(),
			charities.getCurrent()
		).spread(function(newResults, leaders,wleaders, totalDonated, currentCharity) {
			message.leaders = leaders;
			merchantKey.wleaders = leaders;
			message.newResults = _.map(newResults, spinPresenter);
			message.totalDonated = totalDonated;
			message.currentCharity = currentCharity;
			return newResults.length > 0 ? casino.getTotalSpins() : 0;
		}).then(function(total) {
			if (message.newResults.length > 0) {
				lastTimestamp = new Date(message.newResults[0].timestamp);
				message.totalSpins = total;

				io.sockets.emit("newStats", message);
			}
		}).catch(errorHandlerFactory('Error getting results since'));

		// 3. check daily seed hash and announce it if changed
		dailySeeds.getCurrentSeed().then(function(dailySeed) {
			if (!lastCheckedDailySeed || dailySeed.id != lastCheckedDailySeed.id) {
        announceDailySeedHash();
        lastCheckedDailySeed = dailySeed;
			}
		}).catch(errorHandlerFactory('Error checking daily seed'))

	}, 2000);

}

function checkDailySeedExists() {
	return dailySeeds.getCurrentSeed().then(function(seed) {
		if (!seed || !moment().isSame(moment(seed.timestamp), 'day')) {
			return dailySeeds.generateNewSeed();
		}
	}).then(function(result) {
		if (result) {
			console.log('Generated new seed because the last one is more than a day old');
		}
	});
}
