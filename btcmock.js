var express = require('express')
var app = express()
  , server = require('http').createServer(app)
  , utils = require('./roulette/utils')
  , Promise = require('bluebird')
  , _ = require('lodash')
  , Utils = require('./roulette/utils')
  , Colors = require('colors')
  , Chance = new require('chance')
  , expressValidator = require('express-validator')
  , os = require('os')
  , Knex = require('knex')
  , APIClient = require('./roulette/api-client').APIClient;

var users, casino;

var config = require(process.env.ATALAYA_CIRCLE_CONFIG || './config');


var errorHandlerFactory = config.debug ?
	Utils.developmentErrorHandlerFactory :
	Utils.productionErrorHandlerFactory;

/*
	Application initializer
*/
function initApp(db) {
	return Promise.all([
		require('./roulette/users')(db),
		require('./roulette/casino')(db),
	]).then(function(results) {
		users = results[0];
		casino = results[1];
		server.listen(config.btcApiPort);
		console.info(('BTCMock started on port ' + config.btcApiPort).green);
		return app;
	}).catch(errorHandlerFactory('Error while initializing'));
}

/*
	App initialization entry point for unit tests.
*/
module.exports = initApp;

/*
	MAIN. Called when the module is run directly by node.
*/
if (require.main === module) {
	var db = Knex.initialize(config.db);
	module.exports(db);
}


var chance = new Chance(function() { return Math.random(); });

var configModuleName = process.env.ATALAYA_CIRCLE_CONFIG || './config';
var config = require(configModuleName);

var gameClient = new APIClient('http://localhost:' + config.backendPort);

var errorHandlerFactory = config.debug ? utils.developmentErrorHandlerFactory : utils.productionErrorHandlerFactory;

/*
	Confirming or failing transaction.
	For the sake of testing, every second deposit or withdrawal fails. This
	binary flag tells the code whether we should fail or confirm. It is flipped
	to the opposit every fail/confirm.
*/
var doConfirm = true;


/*
	CORS middleware. Allows anyone.
*/
var allowCrossDomain = function(req, res, next) {
	var origin = req.get('origin');
	var allow = true;

	if (allow) {
	    res.header('Access-Control-Allow-Origin', origin);
	    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	    res.header('Access-Control-Allow-Headers', 'Content-Type');
	}

    next();
}


/*
 Express JS endpoints:
*/
app.configure(function() {
	app.use(express.bodyParser());
	app.use(allowCrossDomain);
	app.use(expressValidator([]));
	app.use(app.router);
    app.use('/js', express.static(__dirname + '/frontend/js'));
    app.use('/img', express.static(__dirname + '/frontend/img'));
    app.use('/css', express.static(__dirname + '/frontend/css'));
});


app.get('/', function (req, res) {
  res.sendfile(__dirname + '/frontend/btcmock.html');
});

app.get('/partials/:part', function (req, res) {
  res.sendfile(__dirname + '/frontend/partials/' + req.param('part'));
});


/*
	Allocates and returns a new wallet ID for a new user.
*/
app.get('/rest/public/front_wallets/next', function(req, res) {
	var walletId = utils.generateSessionId();
	console.log('Allocated new wallet address: ' + walletId);
	res.json(200, {
		address: walletId,
		host: os.hostname()
	});
});

/*
	Initiate withdrawal from a front wallet to a destination wallet.
*/
app.post('/rest/public/front_wallets/:id/withdrawals', function(req, res) {
	var destination_id = req.body.destinationAddress;
	var amount = parseInt(req.body.amountNano);
	var transactionId = req.body.transactionId;

	if (destination_id === undefined)
		return res.json(400, {message: 'No Destination ID provided'});
	if (amount === undefined)
		return res.json(400, {message: 'No amount provided'});
	if (isNaN(amount))
		return res.json(400, {message: 'Amount must be a number'})
	if (amount <= 0)
		return res.json(400, {message: 'Amount must be positive'})
	if (!transactionId)
		return res.json(400, {message: 'No transactionId provided'});

	console.log('Requested withdrawal from host: %s', req.headers.host);
	console.log('Transferring %s from front wallet %s to destination wallet %s',
		amount, req.params.id, destination_id);

	// report transaction ID back
	res.json({
		transactionId: transactionId
	})

	// sleep for a second and confirm or fail transaction
	setTimeout(function() {
		gameClient.post(doConfirm ? '/bitcoin/confirm' : '/bitcoin/invalidate', {
			transactionId: transactionId
		}).then(function(response) {
			if (!response.ok) throw response.body;
			console.log((doConfirm ? 'Confirmed' : 'invalidated') + ' transaction ' + transactionId);
		}).catch(errorHandlerFactory('Error')).finally(function() {
			doConfirm = !doConfirm;
		});
	}, 1000);

});

/*
	Deposit bitcoins to a front wallet address.

	XXX This endpoint does not exist on real BTC processor. It mimicks
	a callback from the Bitcoin network when something is deposited to
	a wallet. It is here to be able to deposit from the BTC Mock UI.
*/
app.post('/deposit', function(req, res) {
	var walletId = req.body.walletId;
	var amount = req.body.amount;
	var txId = utils.generateSessionId();

	console.log('Depositing %s BTC to front wallet %s', amount ,walletId);

	gameClient.post('/bitcoin/deposit', {
		walletId:  walletId,
		amount: amount,
		transactionId: txId
	}).then(function(response) {
		if (!response.ok) throw response.body;

		res.json({
			message: 'ok'
		});

		// sleep for a second and confirm or fail transaction
		setTimeout(function() {
			gameClient.post(doConfirm ? '/bitcoin/confirm' : '/bitcoin/invalidate', {
				transactionId: txId
			}).then(function(response) {
				if (!response.ok) throw response.body;
				console.log((doConfirm ? 'Confirmed' : 'invalidated') + ' transaction ' + txId);
			}).catch(errorHandlerFactory('Error')).finally(function() {
				doConfirm = !doConfirm;
			});
		}, 1000);

	}).catch(function(err) {
		res.json(500, err);
		errorHandlerFactory('Error depositing: ')(err.message || err);
	});
});


function generateBTCTransaction(params) {
	var notificationState = params.notificationState ? (
			typeof params.notificationState === 'string' ?
				params.notificationState : params.notificationState[0]
		) : 'ACKNOWLEDGED';

	var randomLedger = Math.random() > 0.5 ? 'IN' : 'OUT';

	return {
		id: utils.generateUUID(),
		timestamp: (params.fromTimestamp || params.toTimestamp || new Date(chance.timestamp())).toISOString(),
		state: params.state || 'BLOCKCHAIN',
		fromAddress: utils.generateUUID(),
		toAddress: utils.generateUUID(),
		value: parseInt(utils.getRandomNumber(Math.pow(10, 8))),
		ledger: params.ledger || randomLedger,
		notificationState: notificationState,
		hash: params.hash || utils.generateUUID()
	}
}

function generateBTCWallet(params) {

	return {
		id: utils.generateUUID(),
		address: utils.generateUUID(),
		toAddress: utils.generateUUID(),
		balanceNanoConfirmed: parseInt(utils.getRandomNumber(Math.pow(10, 8))),
		balanceNanoEstimated: parseInt(utils.getRandomNumber(Math.pow(10, 8)))
	}
}

app.get('/rest/public/transactions', function(req, res) {
	// 1. validate and sanitize params
	if (req.query.hash)
		req.assert('hash', 'Invalid string').notEmpty();

	if (req.query.ledger)
		req.assert('ledger', 'Invalid ledger value').isIn(['IN', 'OUT', 'FEE']);

	if (req.query.state)
		req.assert('state', 'Invalid value').isIn([
			'PENDING', 'PEER_ANNOUNCED', 'BLOCKCHAIN', 'FAILED']);

	if (req.query.notificationState) {
		var states = ['ACKNOWLEDGED', 'PENDING', 'FAILED', 'DONOTNOTIFY'];
		if (typeof req.query.notificationState === 'string')
			req.assert('notificationState', 'Invalid value').isIn(states);
		else {
			_.each(req.query.notificationState, function(s, idx) {
				req.assert('notificationState.' + idx, 'Invalid value').isIn(states);
			});
		}
	}

	if (req.query.fromTimestamp) {
		req.assert('fromTimestamp', 'Invalid format').isDate();
		req.sanitize('fromTimestamp').toDate();
	}

	if (req.query.toTimestamp) {
		req.assert('toTimestamp', 'Invalid format').isDate();
		req.sanitize('toTimestamp').toDate();
	}

	var errors = req.validationErrors();
	if (errors) {
		return res.json(400, errors);
	}

	// 3. number of records to generate
	var nRecords = parseInt(utils.getRandomNumber(10));

	// 4. generate records
	var result = [];
	_.times(nRecords, function() {
		result.push(generateBTCTransaction(req.query));
	})

	res.json(result);
});

/*
	Return front wallets. Query parameters:

	* 'limit' - number of records to return (default 50)
	* 'offset' - skip 'offset' records
	* 'positiveBalanceOnly' - if set to true, return only ones with positive balance.
	* 'address' - return only wallet for this address.
*/
app.get('/rest/private/front_wallets', function(req, res) {

	// 1. get users from the db
	users.getAll(req.query.limit || 50, req.query.offset)
		.then(function(users) {
			// 2. return wallets

			if (req.query.address) {
				users = _.filter(users, function(u) {
					return u.wallet_id == req.query.address;
				});
			}

			res.json(_.map(users, function(user) {
				return {
					id: utils.generateUUID(),
					address: user.wallet_id,
					balanceNanoConfirmed: parseInt(utils.getRandomNumber(Math.pow(10, 8))),
					balanceNanoEstimated: parseInt(utils.getRandomNumber(Math.pow(10, 8)))
				}
			}));
		});
});

app.get('/rest/private/main_wallets', function(req, res) {


	// 1. number of records to generate
	var nRecords = parseInt(utils.getRandomNumber(10));

	// 2. generate records
	var result = [];
	_.times(nRecords, function() {
		result.push(generateBTCWallet(req.query));
	})

	res.json(result);
});


app.put('/rest/private/front_wallets/:address/empty', function(req, res) {
	console.log(req.body)
	res.json(200, {});
});
