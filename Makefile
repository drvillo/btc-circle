.PHONY: test

test:
	mocha -R spec
run:
	nodemon app.js

run-backend:
	nodemon back.js

run-btcmock:
	nodemon btcmock.js

run-tasks:
	node tasks.js

compile-css:
	node -e 'var assets = require("./assets"); assets.compileCSS(process.exit)'

compile-js:
	node -e 'var assets = require("./assets"); assets.compileJS(process.exit)'

compile-assets:
	node -e 'var assets = require("./assets"); assets.compile(process.exit)'

deploy-assets-%: compile-assets
	aws --profile circle s3 sync frontend/css s3://$*/css
	aws --profile circle s3 sync frontend/fonts s3://$*/fonts
	aws --profile circle s3 sync frontend/img/$* s3://$*/img
	aws --profile circle s3 sync frontend/js s3://$*/js

