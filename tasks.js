/*
  These tasks are to be executed daily via CRON.

  The tasks are:
    1. Generate new daily seed.
    2. Generate daily session IDs for all users.
*/

var Knex = require('knex')
  , Promise = require('bluebird')
  , moment = require('moment');

var config = require(process.env.ATALAYA_CIRCLE_CONFIG || './config');

function runApp(db) {
  var users, dailySeeds;

  return Promise.join(
    require('./roulette/users')(db),
    require('./roulette/seeds')(db, !config.production)
  ).spread(function(_users, _dailySeeds) {
    users = _users;
    dailySeeds = _dailySeeds;

    return dailySeeds.generateNewSeed()
      .then(function() {
        console.log('New seed generated');
      }).catch(function(err) {
        if (err.clientError.code == 'SQLITE_CONSTRAINT' ||
          err.clientError.cause.code == 'ER_DUP_ENTRY') {
          console.log('Seed for this day already exists. Skipping.')
        } else {
          throw err;
        }
      });
  }).then(function() {
    console.log('Generating daily user session IDs.');
    return users.updateDailySessionIds().then(function() {
      console.log('New daily user sesion IDs generated');
    });
  }).then(function() {
    return users.removeOrphansOlderThan(moment().subtract('days', 7).toDate()).then(function(count) {
      console.log(count + ' orphans removed');
    });
  }).then(function() {
    console.log('Done!');
    process.exit(0);
  }).catch(function(err) {
    console.error("Error running daily tasks: " + err);
    if (err.stack) console.log(err.stack);
    process.exit(1);
  });
}

/*
  App initialization entry point for unit tests.
*/
module.exports = runApp;

if (require.main === module) {
  var db = Knex.initialize(config.db);
  module.exports(db);
}
