# Atalaya roulette

## Quickstart

Make sure you have `node` and `npm` installed.

Make sure you install `qrencode` (`apt-get install qrencode` or `brew install qrencode`).

Install `nodemon` globally:

```
$ npm install -g nodemon
```

Install dependencies:

```
$ cd atalaya-circle
$ npm install
```

Run Game server (port *8000*):

```
$ make run
```

Run Game internal API server. This is the one Bitcoin processor talks to (port *8001*).

```
$ make run-backend
```

Run BTC Processor mock server. This is a fake Bitcoin processor (port *9998*).

```
$ make run-btcmock
```

Go to [http://localhost:8000/](http://localhost:8000/) and play.
Try to open several browsers at the same time and see beauty of Web Sockets.

There is a `tasks.js` app that is supposed to run daily. It can be run manually in development environment
and shoud be set up to be run under CRON in production. It depends on the same `config.js` config
file as the other apps. To run it in development environment do:

```
$ make run-tasks
```


## Themes

It is possible to change the theme and name of the game. There are two
variables in the config file that control it:

```javascript
{
	gameId: 'alwaysLooseGame',
	gameTitle: 'Always Loose Here',
}
```

CSS and image files for our **alwaysLooseGame** will have to be located in:
`frontend/less/alwaysLooseGame/circle.less` and `frontend/img/alwaysLooseGame/*`
. Compiled CSS will be saved to `frontend/css/alwaysLooseGame.css`.

`gameTitle` will be rendered as the game name in the HTML.

To make sure that CSS for a particular game is compiled, add its ID
to `GamesIDs` in `assets.js`:

```javascript
var GamesIDs = ['goodwillcircle', 'alwaysLooseGame'];
```

## Running in Production

To run the game in production, make sure:

 - Less is compiled into CSS and Javascript files are minified and combined
into one file. To do so, run:

```bash
make compile-assets
```

 - Config file has the following variables set:

```javascript
{
	debug: false,
	production: true
}
```

## Sync assets to CDN

Assets are stored on Amazon S3 and served via Amazon CloudFront. Assets
can be synced with production by running

```bash
make deploy-assets-<gameId>
```

If running first time, install *aws cli* tools (```pip install awscli```) and
configure security credentials by adding the following lines to `~/.aws/config`:

```
[profile circle]
aws_access_key_id = XXXXXXX
aws_secret_access_key = YYYYYYY
output = json
region = us-east-1
```

## Endpoints

### Public

#### PUT /personal_wallet

Update personal wallet. The wallet where the user sends money from.
If the user did not have front wallet, a new one will be allocated.

Params (JSON):

```json
{
	"wallet_id": "123" // user's personal wallet
}
```

Responds with 200 and a user object.

#### POST /login

Authenticates the user. Updates the session with this user ID.

Params (JSON):

```json
{
	"username": "john", // user name, required
	"password": "secret" // password, required
}
```

If user has been authenticated, responds with `200` and:

```json
{
	"id": 1, // user ID
	"name": "john" // user name
}
```

Also triggers a websocket message to user with user's wallet, balance, etc.

Responds with `401` if user not found or `409` if password is invalid.


#### POST /logout

Logs user out. Accepts no parameters, the user ID is already in the session.
A new anonymous user session is created after logging out.

#### POST /signup

Create a new permanent user out of current anonymous user.

Params (JSON):

```json
{
	"username": "john", // username, required
	"password": "secret" // password, required
}
```

If successful a `200` response with the following body returned:

```json
{
	"id": 1, // user ID
	"name": "john" // user name
}
```

The ID of anonymous user is reserved for the new permanent user. No changes to
session are done.

Returns `409` if this `username` has already been taken.
Returns `400` if `username` is shorter than 6 characters.

#### POST /spin

Spins the wheel, creating a new transaction record for the user.

Params (JSON):

```json
{
	"bet": 0.001, // bet size in BTC, required
	"circle_id": 1, // ID of the circle played, optional (default to 1)
	"client_seed": "asdf123" // client seed string, required
}
```

If successfull responds with `200` and body:

```json
{
	"sector": 25, // winning sector number
	"returned": 0.075, // amount of bitcoins returned to the user as a result of the spin
	"bet": 0.1, // bet in bitcoins
	"balance": 3.5, // user balance after the spin
	"angle": 135, // angle of the wheel in degrees - used to render the wheel spin.
	"dummy": true // if player's balance is 0, the spin is not recorded anywhere.
}
```

Returns `400` if parameters are missing or incorrect.
Returns `412` if some preconditions are not met: the amount is more than user balance,
circle ID is not known, daily seed is missing.

#### POST /withdraw

Deposits remaining confirmed balance from the user's game wallet back to the wallet where he
deposited the money from. (Do we have an option when user manually specifies the wallet to
transfer the money to?)

Endpoint takes no parameters.

Params (JSON):

```json
{
	"destination_wallet_id": "123abc" // destination wallet ID.
}
```

Returns `200` if withdrawal was successfull with body:

```json
{
	"balance": 0.0 // current balance, should be 0
}
```

#### WebSocket messages from server to clients:

	* `dailySeedHash` announces new daily seed hash
		```
			123deadbeef123
		```

	* `userDetails` whenever user details change, this message is sent to the user
		```
		{
			"name": "john",	// username
			"id":	1,	// user ID
			"wallet": "123abcd",	// wallet ID
			"balance": 2.3 // user balance in BTC
		}
		```

	* `recentStats` broadcasted to all new connections once they are connected.
	```
	{
		"totalSpins": 12345, // total spins in the casino
		"recentStats": [ // 100 recent spins
			{
				"user_id": 1, // ID of the user
				"timestamp": 123456, // Unix timestamp of the spin
				"bet": 0.1, // bet amount
				"returned": 0.1, // amount returned
				"outcome": 0, // winning sector value
				"wheel_id": 1, // wheel played
				"sector": 25, // sector number
			}
		]
	}
	```

	* `newStats` broadcasted to all active connections every X seconds.
		Format is the same as above in `recentStats`, but `recentStats` field
		is called `newStats` and contain all spins since the last announcement.


### Private

#### GET /users

Returns a list of all users (anonymous and registered) in the database.


#### POST /bitcoin/deposit

Notify the game that money have arrived at the user's wallet. The transaction is not confirmed yet.

Params (JSON):

```json
{
	"amount": 3, // amount in BTC
	"walletId": "123abc", // game front wallet ID of the user
	"transactionId": "123123123", // unique transaction ID. Must be unique in the BTC infrastructure
}
```

If all correct,  responds with `202` and body:

```json
{"message": "amount accepted as not confirmed"}
```

Responds with `404` if no such `toWallet` found.
Responds with `400` if fields above are missing or amount is not correct (e.g. negative)

#### POST /bitcoin/confirm

Notify the game that money for transaction have been confirmed (both deposit and withdrawal).

Params (JSON):
```json
{
	"transactionId": 12345, // unique transaction ID. Must be unique in the BTC infrastructure
}
```

Responds with `201` if all went well.
Responds with `404` if no such "transactionId" found or it has already been confirmed.
Responds with `400` if transaction ID is missing.

#### POST /bitcoin/invalidate

Notify the game that transaction has failed and will never be confirmed.

Params and response is the same as for `/bitcoin/confirm`.


### BTC Processor

#### GET /rest/public/front_wallets/next

Allocate a new front wallet address

Takes no parameters.

Responds with `200` and body:
```json
{
	"address": "xxx" // unique wallet address
}
```

#### POST /front_wallets/<id>/withdrawals

Initiate a withdrawal from a front wallet.

URL param `id` - address of the front wallet.

Params (JSON):

```json
{
	"destinationAddress": "123xxx", // address of the wallet where the money will be transferred to.
	"frontWalletAddress": "xxyy", // front wallet address
	"amountNano": 11, // amount to transfer in nanocoins.
	"transactionId": "123abc" // unique transaction to track this withdrawal
}
```

Responds with `200` and body:
```json
{
	"transactionId": "xxx" // unique transaction id that is sent as a parameter above
}
```

